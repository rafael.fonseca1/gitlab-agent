# Generated by the protocol buffer compiler.  DO NOT EDIT!
# Source: internal/module/agent_tracker/rpc/rpc.proto for package 'gitlab.agent.agent_tracker.rpc'

require 'grpc'
require 'internal/module/agent_tracker/rpc/rpc_pb'

module Gitlab
  module Agent
    module AgentTracker
      module Rpc
        module AgentTracker
          class Service

            include ::GRPC::GenericService

            self.marshal_class_method = :encode
            self.unmarshal_class_method = :decode
            self.service_name = 'gitlab.agent.agent_tracker.rpc.AgentTracker'

            rpc :GetConnectedAgents, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsRequest, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsResponse
            rpc :GetConnectedAgentsByProjectIds, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByProjectIDsRequest, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByProjectIDsResponse
            rpc :GetConnectedAgentsByProjectIDs, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByProjectIDsRequest, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByProjectIDsResponse
            rpc :GetConnectedAgentsByAgentIds, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByAgentIDsRequest, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByAgentIDsResponse
            rpc :GetConnectedAgentsByAgentIDs, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByAgentIDsRequest, ::Gitlab::Agent::AgentTracker::Rpc::GetConnectedAgentsByAgentIDsResponse
            rpc :CountAgentsByAgentVersions, ::Gitlab::Agent::AgentTracker::Rpc::CountAgentsByAgentVersionsRequest, ::Gitlab::Agent::AgentTracker::Rpc::CountAgentsByAgentVersionsResponse
          end

          Stub = Service.rpc_stub_class
        end
      end
    end
  end
end
