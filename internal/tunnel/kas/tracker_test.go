package kas

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/redistool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_redis"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/testhelpers"
	"go.uber.org/mock/gomock"
	clocktesting "k8s.io/utils/clock/testing"
)

var (
	_ Registerer = &RedisTracker{}
	_ Tracker    = &RedisTracker{}
	_ Querier    = &RedisTracker{}
)

const (
	selfURL = "grpc://1.1.1.1:10"
	ttl     = time.Minute
)

func TestRegisterConnection(t *testing.T) {
	ctrl := gomock.NewController(t)
	hash := mock_redis.NewMockExpiringHashAPI[int64, string](ctrl)
	b := mock_redis.NewMockSetBuilder[int64, string](ctrl)
	tm := time.Now()
	r := &RedisTracker{
		ownPrivateAPIURL: selfURL,
		clock:            clocktesting.NewFakePassiveClock(tm),
		tunnelsByAgentID: hash,
	}
	gomock.InOrder(
		hash.EXPECT().
			SetBuilder().
			Return(b),
		b.EXPECT().
			Set(testhelpers.AgentID, gomock.Any(), gomock.Any()).
			Do(func(key int64, ttl time.Duration, kvs ...redistool.BuilderKV[string]) {
				require.Len(t, kvs, 1)
				assert.Equal(t, selfURL, kvs[0].HashKey)
				assert.Equal(t, tm.Add(ttl).Unix(), kvs[0].Value.ExpiresAt)
			}),
		b.EXPECT().
			Do(gomock.Any()),
	)
	assert.NoError(t, r.RegisterTunnel(context.Background(), ttl, testhelpers.AgentID))
}

func TestUnregisterConnection(t *testing.T) {
	ctrl := gomock.NewController(t)
	hash := mock_redis.NewMockExpiringHashAPI[int64, string](ctrl)
	b := mock_redis.NewMockSetBuilder[int64, string](ctrl)
	r := &RedisTracker{
		ownPrivateAPIURL: selfURL,
		clock:            clocktesting.NewFakePassiveClock(time.Now()),
		tunnelsByAgentID: hash,
	}
	gomock.InOrder(
		hash.EXPECT().
			SetBuilder().
			Return(b),
		b.EXPECT().
			Set(testhelpers.AgentID, gomock.Any(), gomock.Any()),
		b.EXPECT().
			Do(gomock.Any()),
		hash.EXPECT().
			Unset(gomock.Any(), testhelpers.AgentID, selfURL),
	)

	assert.NoError(t, r.RegisterTunnel(context.Background(), ttl, testhelpers.AgentID))
	assert.NoError(t, r.UnregisterTunnel(context.Background(), testhelpers.AgentID))
}

func TestUnregisterConnection_TwoConnections(t *testing.T) {
	ctrl := gomock.NewController(t)
	hash := mock_redis.NewMockExpiringHashAPI[int64, string](ctrl)
	b1 := mock_redis.NewMockSetBuilder[int64, string](ctrl)
	b2 := mock_redis.NewMockSetBuilder[int64, string](ctrl)
	r := &RedisTracker{
		ownPrivateAPIURL: selfURL,
		clock:            clocktesting.NewFakePassiveClock(time.Now()),
		tunnelsByAgentID: hash,
	}
	gomock.InOrder(
		hash.EXPECT().
			SetBuilder().
			Return(b1),
		b1.EXPECT().
			Set(testhelpers.AgentID, gomock.Any(), gomock.Any()),
		b1.EXPECT().
			Do(gomock.Any()),
		hash.EXPECT().
			SetBuilder().
			Return(b2),
		b2.EXPECT().
			Set(testhelpers.AgentID, gomock.Any(), gomock.Any()),
		b2.EXPECT().
			Do(gomock.Any()),
		hash.EXPECT().
			Unset(gomock.Any(), testhelpers.AgentID, selfURL),
		hash.EXPECT().
			Unset(gomock.Any(), testhelpers.AgentID, selfURL),
	)

	assert.NoError(t, r.RegisterTunnel(context.Background(), ttl, testhelpers.AgentID))
	assert.NoError(t, r.RegisterTunnel(context.Background(), ttl, testhelpers.AgentID))
	assert.NoError(t, r.UnregisterTunnel(context.Background(), testhelpers.AgentID))
	assert.NoError(t, r.UnregisterTunnel(context.Background(), testhelpers.AgentID))
}

func TestKASURLsByAgentID_HappyPath(t *testing.T) {
	r, hash := setupTracker(t)
	hash.EXPECT().
		ScanAndGC(gomock.Any(), testhelpers.AgentID, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			var done bool
			done, err := cb(selfURL, nil, nil)
			if err != nil || done {
				return 0, err
			}
			return 0, nil
		})
	kasURLs, err := r.KASURLsByAgentID(context.Background(), testhelpers.AgentID)
	require.NoError(t, err)
	assert.Equal(t, []string{selfURL}, kasURLs)
}

func TestKASURLsByAgentID_ScanError(t *testing.T) {
	r, hash := setupTracker(t)
	hash.EXPECT().
		ScanAndGC(gomock.Any(), testhelpers.AgentID, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			done, err := cb("", nil, errors.New("intended error"))
			require.NoError(t, err)
			assert.False(t, done)
			return 0, nil
		})
	kasURLs, err := r.KASURLsByAgentID(context.Background(), testhelpers.AgentID)
	assert.EqualError(t, err, "intended error")
	assert.Empty(t, kasURLs)
}

func TestRefresh_NoAgents(t *testing.T) {
	ctrl := gomock.NewController(t)
	hash := mock_redis.NewMockExpiringHashAPI[int64, string](ctrl)
	b := mock_redis.NewMockSetBuilder[int64, string](ctrl)
	r := &RedisTracker{
		ownPrivateAPIURL: selfURL,
		clock:            clocktesting.NewFakePassiveClock(time.Now()),
		tunnelsByAgentID: hash,
	}
	gomock.InOrder(
		hash.EXPECT().
			SetBuilder().
			Return(b),
		b.EXPECT().
			Do(gomock.Any()),
	)
	assert.NoError(t, r.Refresh(context.Background(), ttl))
}

func TestRefresh_OneAgent(t *testing.T) {
	ctrl := gomock.NewController(t)
	hash := mock_redis.NewMockExpiringHashAPI[int64, string](ctrl)
	b := mock_redis.NewMockSetBuilder[int64, string](ctrl)
	tm := time.Now()
	r := &RedisTracker{
		ownPrivateAPIURL: selfURL,
		clock:            clocktesting.NewFakePassiveClock(tm),
		tunnelsByAgentID: hash,
	}
	gomock.InOrder(
		hash.EXPECT().
			SetBuilder().
			Return(b),
		b.EXPECT().
			Set(testhelpers.AgentID, gomock.Any(), gomock.Any()).
			Do(func(key int64, ttl time.Duration, kvs ...redistool.BuilderKV[string]) {
				require.Len(t, kvs, 1)
				assert.Equal(t, selfURL, kvs[0].HashKey)
				assert.Equal(t, tm.Add(ttl).Unix(), kvs[0].Value.ExpiresAt)
			}),
		b.EXPECT().
			Do(gomock.Any()),
	)
	assert.NoError(t, r.Refresh(context.Background(), ttl, testhelpers.AgentID))
}

func TestRefresh_TwoAgents(t *testing.T) {
	ctrl := gomock.NewController(t)
	hash := mock_redis.NewMockExpiringHashAPI[int64, string](ctrl)
	b := mock_redis.NewMockSetBuilder[int64, string](ctrl)
	tm := time.Now()
	r := &RedisTracker{
		ownPrivateAPIURL: selfURL,
		clock:            clocktesting.NewFakePassiveClock(tm),
		tunnelsByAgentID: hash,
	}
	gomock.InOrder(
		hash.EXPECT().
			SetBuilder().
			Return(b),
		b.EXPECT().
			Set(testhelpers.AgentID, gomock.Any(), gomock.Any()).
			Do(func(key int64, ttl time.Duration, kvs ...redistool.BuilderKV[string]) {
				require.Len(t, kvs, 1)
				assert.Equal(t, selfURL, kvs[0].HashKey)
				assert.Equal(t, tm.Add(ttl).Unix(), kvs[0].Value.ExpiresAt)
			}),
		b.EXPECT().
			Set(testhelpers.AgentID+1, gomock.Any(), gomock.Any()).
			Do(func(key int64, ttl time.Duration, kvs ...redistool.BuilderKV[string]) {
				require.Len(t, kvs, 1)
				assert.Equal(t, selfURL, kvs[0].HashKey)
				assert.Equal(t, tm.Add(ttl).Unix(), kvs[0].Value.ExpiresAt)
			}),
		b.EXPECT().
			Do(gomock.Any()),
	)
	assert.NoError(t, r.Refresh(context.Background(), ttl, testhelpers.AgentID, testhelpers.AgentID+1))
}

func setupTracker(t *testing.T) (*RedisTracker, *mock_redis.MockExpiringHashAPI[int64, string]) {
	ctrl := gomock.NewController(t)
	hash := mock_redis.NewMockExpiringHashAPI[int64, string](ctrl)
	return &RedisTracker{
		ownPrivateAPIURL: selfURL,
		clock:            clocktesting.NewFakePassiveClock(time.Now()),
		tunnelsByAgentID: hash,
	}, hash
}
