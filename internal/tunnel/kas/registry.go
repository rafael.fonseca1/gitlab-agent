package kas

import (
	"context"
	"sync/atomic"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/tunserver"
	"go.opentelemetry.io/otel/attribute"
	otelcodes "go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/util/wait"
)

const (
	stopTimeout = 5 * time.Second
	stripeBits  = 8

	traceTunnelFoundAttr    attribute.Key = "found"
	traceStoppedTunnelsAttr attribute.Key = "stoppedTunnels"
	traceAbortedFTRAttr     attribute.Key = "abortedFTR"
)

type Handler interface {
	// HandleTunnel is called with server-side interface of the reverse tunnel.
	// It registers the tunnel and blocks, waiting for a request to proxy through the tunnel.
	// The method returns the error value to return to gRPC framework.
	// ageCtx can be used to unblock the method if the tunnel is not being used already.
	HandleTunnel(ageCtx context.Context, agentInfo *api.AgentInfo, server rpc.ReverseTunnel_ConnectServer) error
}

type Registry struct {
	log           *zap.Logger
	api           modshared.API
	tracer        trace.Tracer
	refreshPeriod time.Duration
	stripes       syncz.StripedValue[registryStripe]
}

func NewRegistry(log *zap.Logger, api modshared.API, tracer trace.Tracer, refreshPeriod, ttl time.Duration,
	tunnelTracker Tracker) *Registry {
	return &Registry{
		log:           log,
		api:           api,
		tracer:        tracer,
		refreshPeriod: refreshPeriod,
		stripes: syncz.NewStripedValueInit(stripeBits, func() registryStripe {
			return registryStripe{
				log:                   log,
				api:                   api,
				tracer:                tracer,
				tunnelTracker:         tunnelTracker,
				ttl:                   ttl,
				tunsByAgentID:         make(map[int64]agentID2tunInfo),
				findRequestsByAgentID: make(map[int64]map[*findTunnelRequest]struct{}),
			}
		}),
	}
}

func (r *Registry) FindTunnel(ctx context.Context, agentID int64, service, method string) (bool, tunserver.FindHandle) {
	// Use GetPointer() to avoid copying the embedded mutex.
	return r.stripes.GetPointer(agentID).FindTunnel(ctx, agentID, service, method)
}

func (r *Registry) HandleTunnel(ageCtx context.Context, agentInfo *api.AgentInfo, server rpc.ReverseTunnel_ConnectServer) error {
	// Use GetPointer() to avoid copying the embedded mutex.
	return r.stripes.GetPointer(agentInfo.ID).HandleTunnel(ageCtx, agentInfo, server)
}

func (r *Registry) KASURLsByAgentID(ctx context.Context, agentID int64) ([]string, error) {
	ctx, span := r.tracer.Start(ctx, "Registry.KASURLsByAgentID", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	// Use GetPointer() to avoid copying the embedded mutex.
	return r.stripes.GetPointer(agentID).tunnelTracker.KASURLsByAgentID(ctx, agentID)
}

func (r *Registry) Run(ctx context.Context) error {
	defer r.stopInternal(ctx)
	refreshTicker := time.NewTicker(r.refreshPeriod)
	defer refreshTicker.Stop()
	done := ctx.Done()
	for {
		select {
		case <-done:
			return nil
		case <-refreshTicker.C:
			r.refreshRegistrations(ctx)
		}
	}
}

// stopInternal aborts any open tunnels.
// It should not be necessary to abort tunnels when registry is used correctly i.e. this method is called after
// all tunnels have terminated gracefully.
func (r *Registry) stopInternal(ctx context.Context) (int /*stoppedTun*/, int /*abortedFtr*/) {
	ctx, cancel := context.WithTimeout(context.WithoutCancel(ctx), stopTimeout)
	defer cancel()
	ctx, span := r.tracer.Start(ctx, "Registry.stopInternal", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	var wg wait.Group
	var stoppedTun, abortedFtr atomic.Int32

	for s := range r.stripes.Stripes { // use index var to avoid copying embedded mutex
		s := s
		wg.Start(func() {
			stopCtx, stopSpan := r.tracer.Start(ctx, "registryStripe.Stop", trace.WithSpanKind(trace.SpanKindInternal))
			defer stopSpan.End()

			st, aftr := r.stripes.Stripes[s].Stop(stopCtx)
			stoppedTun.Add(int32(st))
			abortedFtr.Add(int32(aftr))
			stopSpan.SetAttributes(traceStoppedTunnelsAttr.Int(st), traceAbortedFTRAttr.Int(aftr))
		})
	}
	wg.Wait()

	v1 := int(stoppedTun.Load())
	v2 := int(abortedFtr.Load())
	span.SetAttributes(traceStoppedTunnelsAttr.Int(v1), traceAbortedFTRAttr.Int(v2))
	return v1, v2
}

func (r *Registry) refreshRegistrations(ctx context.Context) {
	ctx, span := r.tracer.Start(ctx, "Registry.refreshRegistrations", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	for s := range r.stripes.Stripes { // use index var to avoid copying embedded mutex
		func() {
			refreshCtx, refreshSpan := r.tracer.Start(ctx, "registryStripe.Refresh", trace.WithSpanKind(trace.SpanKindInternal))
			defer refreshSpan.End()

			err := r.stripes.Stripes[s].Refresh(refreshCtx)
			if err != nil {
				r.api.HandleProcessingError(refreshCtx, r.log, modshared.NoAgentID, "Failed to refresh data", err)
				refreshSpan.SetStatus(otelcodes.Error, "Failed to refresh data")
				refreshSpan.RecordError(err)
				// fallthrough
			} else {
				refreshSpan.SetStatus(otelcodes.Ok, "")
			}
		}()
	}
}
