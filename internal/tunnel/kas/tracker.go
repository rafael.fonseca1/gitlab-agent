package kas

import (
	"context"
	"errors"
	"time"

	"github.com/redis/rueidis"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/redistool"
	otelmetric "go.opentelemetry.io/otel/metric"
	"k8s.io/utils/clock"
)

const (
	tunnelsByAgentIDHashName = "tunnels_by_agent_id"
)

type Querier interface {
	// KASURLsByAgentID returns the list of kas URLs for a particular agent id.
	// A partial list may be returned together with an error.
	// Safe for concurrent use.
	KASURLsByAgentID(ctx context.Context, agentID int64) ([]string, error)
}

// Registerer allows to register and unregister tunnels.
// Caller is responsible for periodically calling GC() and Refresh().
// Not safe for concurrent use.
type Registerer interface {
	// RegisterTunnel registers tunnel with the tracker.
	RegisterTunnel(ctx context.Context, ttl time.Duration, agentID int64) error
	// UnregisterTunnel unregisters tunnel with the tracker.
	UnregisterTunnel(ctx context.Context, agentID int64) error
	// Refresh refreshes registered tunnels in the underlying storage.
	Refresh(ctx context.Context, ttl time.Duration, agentIds ...int64) error
}

type Tracker interface {
	Registerer
	Querier
}

type RedisTracker struct {
	ownPrivateAPIURL string
	clock            clock.PassiveClock
	tunnelsByAgentID redistool.ExpiringHashAPI[int64, string] // agentID -> kas URL -> nil
}

func NewRedisTracker(client rueidis.Client, agentKeyPrefix string, ownPrivateAPIURL string, m otelmetric.Meter) (*RedisTracker, error) {
	tunnelsByAgentID, err := redistool.NewRedisExpiringHashAPI[int64, string](tunnelsByAgentIDHashName, client, tunnelsByAgentIDHashKey(agentKeyPrefix), strToStr, m)
	if err != nil {
		return nil, err
	}
	return &RedisTracker{
		ownPrivateAPIURL: ownPrivateAPIURL,
		clock:            clock.RealClock{},
		tunnelsByAgentID: tunnelsByAgentID,
	}, nil
}

func (t *RedisTracker) RegisterTunnel(ctx context.Context, ttl time.Duration, agentID int64) error {
	b := t.tunnelsByAgentID.SetBuilder()
	b.Set(agentID, ttl, t.kv(t.clock.Now().Add(ttl)))
	return b.Do(ctx)
}

func (t *RedisTracker) UnregisterTunnel(ctx context.Context, agentID int64) error {
	return t.tunnelsByAgentID.Unset(ctx, agentID, t.ownPrivateAPIURL)
}

func (t *RedisTracker) KASURLsByAgentID(ctx context.Context, agentID int64) ([]string, error) {
	var urls []string
	var errs []error
	_, err := t.tunnelsByAgentID.ScanAndGC(ctx, agentID, func(rawHashKey string, value []byte, err error) (bool, error) {
		if err != nil {
			errs = append(errs, err)
			return false, nil
		}
		urls = append(urls, rawHashKey)
		return false, nil
	})
	if err != nil {
		errs = append(errs, err)
	}
	return urls, errors.Join(errs...)
}

func (t *RedisTracker) Refresh(ctx context.Context, ttl time.Duration, agentIds ...int64) error {
	b := t.tunnelsByAgentID.SetBuilder()
	// allocate once. Slice is passed as-is to variadic funcs vs individual args allocate a new one on each call
	kvs := []redistool.BuilderKV[string]{t.kv(t.clock.Now().Add(ttl))}
	for _, agentID := range agentIds {
		b.Set(agentID, ttl, kvs...)
	}
	return b.Do(ctx)
}

func (t *RedisTracker) kv(expiresAt time.Time) redistool.BuilderKV[string] {
	return redistool.BuilderKV[string]{
		HashKey: t.ownPrivateAPIURL,
		Value: &redistool.ExpiringValue{
			ExpiresAt: expiresAt.Unix(),
			Value:     nil, // nothing to store.
		},
	}
}

// tunnelsByAgentIDHashKey returns a key for agentID -> (kasUrl -> nil).
func tunnelsByAgentIDHashKey(agentKeyPrefix string) redistool.KeyToRedisKey[int64] {
	prefix := agentKeyPrefix + ":kas_by_agent_id:"
	return func(agentID int64) string {
		return redistool.PrefixedInt64Key(prefix, agentID)
	}
}

func strToStr(key string) string {
	return key
}
