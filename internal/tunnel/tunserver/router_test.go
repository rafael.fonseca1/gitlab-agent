package tunserver

import (
	"context"
	"io"
	"net"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool/test"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_tool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/tlstool"
	metricnoop "go.opentelemetry.io/otel/metric/noop"
	tracenoop "go.opentelemetry.io/otel/trace/noop"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/testing/protocmp"
	"k8s.io/apimachinery/pkg/util/wait"
)

var (
	_ grpc.StreamHandler = (*Router)(nil).routeToGatewayTunserver
	_ grpc.StreamHandler = (*Router)(nil).routeToTunclient
	_ DataCallback       = (*wrappingCallback)(nil)
)

func TestRouter_UnaryHappyPath(t *testing.T) {
	ctrl := gomock.NewController(t)
	unaryResponse := &test.Response{Message: &test.Response_Scalar{Scalar: 123}}
	routingMeta := routingMetadata()
	payloadMD, responseMD, trailersMD := meta()
	payloadReq := &test.Request{S1: "123"}
	var (
		headerResp  metadata.MD
		trailerResp metadata.MD
	)
	tun := NewMockTunnel(ctrl)
	tun.EXPECT().
		ForwardStream(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		Do(forwardStream(t, routingMeta, payloadMD, payloadReq, unaryResponse, responseMD, trailersMD))
	runRouterTest(t, tun, func(client test.TestingClient) {
		ctx := metadata.NewOutgoingContext(context.Background(), metadata.Join(routingMeta, payloadMD))
		// grpc.Header() and grpc.Trailer are ok here because it's a unary RPC.
		response, err := client.RequestResponse(ctx, payloadReq, grpc.Header(&headerResp), grpc.Trailer(&trailerResp)) //nolint: forbidigo
		require.NoError(t, err)
		assert.Empty(t, cmp.Diff(response, unaryResponse, protocmp.Transform()))
		mdContains(t, responseMD, headerResp)
		mdContains(t, trailersMD, trailerResp)
	})
}

func TestRouter_UnaryImmediateError(t *testing.T) {
	ctrl := gomock.NewController(t)
	routingMeta := routingMetadata()
	statusWithDetails, err := status.New(codes.InvalidArgument, "some expected error").
		WithDetails(&test.Request{S1: "some details of the error"})
	require.NoError(t, err)
	tun := NewMockTunnel(ctrl)
	tun.EXPECT().
		ForwardStream(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		Return(statusWithDetails.Err())
	runRouterTest(t, tun, func(client test.TestingClient) {
		ctx := metadata.NewOutgoingContext(context.Background(), routingMeta)
		_, err = client.RequestResponse(ctx, &test.Request{S1: "123"})
		require.Error(t, err)
		receivedStatus := status.Convert(err).Proto()
		assert.Empty(t, cmp.Diff(receivedStatus, statusWithDetails.Proto(), protocmp.Transform()))
	})
}

func TestRouter_UnaryErrorAfterHeader(t *testing.T) {
	ctrl := gomock.NewController(t)
	routingMeta := routingMetadata()
	payloadMD, responseMD, trailersMD := meta()
	statusWithDetails, err := status.New(codes.InvalidArgument, "some expected error").
		WithDetails(&test.Request{S1: "some details of the error"})
	require.NoError(t, err)
	var (
		headerResp  metadata.MD
		trailerResp metadata.MD
	)
	tun := NewMockTunnel(ctrl)
	tun.EXPECT().
		ForwardStream(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		DoAndReturn(func(log *zap.Logger, rpcAPI modshared.RPCAPI, incomingStream grpc.ServerStream, cb DataCallback) error {
			verifyMeta(t, incomingStream, routingMeta, payloadMD)
			assert.NoError(t, cb.Header(grpctool.MetaToValuesMap(responseMD)))
			assert.NoError(t, cb.Trailer(grpctool.MetaToValuesMap(trailersMD)))
			return statusWithDetails.Err()
		})
	runRouterTest(t, tun, func(client test.TestingClient) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		ctx = metadata.NewOutgoingContext(ctx, metadata.Join(routingMeta, payloadMD))
		// grpc.Header() and grpc.Trailer are ok here because it's a unary RPC.
		_, err := client.RequestResponse(ctx, &test.Request{S1: "123"}, grpc.Header(&headerResp), grpc.Trailer(&trailerResp)) //nolint: forbidigo
		require.Error(t, err)
		receivedStatus := status.Convert(err).Proto()
		assert.Empty(t, cmp.Diff(receivedStatus, statusWithDetails.Proto(), protocmp.Transform()))
		mdContains(t, responseMD, headerResp)
		mdContains(t, trailersMD, trailerResp)
	})
}

func TestRouter_StreamHappyPath(t *testing.T) {
	ctrl := gomock.NewController(t)
	streamResponse := &test.Response{Message: &test.Response_Scalar{Scalar: 123}}
	routingMeta := routingMetadata()
	payloadMD, responseMD, trailersMD := meta()
	payloadReq := &test.Request{S1: "123"}
	tun := NewMockTunnel(ctrl)
	tun.EXPECT().
		ForwardStream(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		Do(forwardStream(t, routingMeta, payloadMD, payloadReq, streamResponse, responseMD, trailersMD))
	runRouterTest(t, tun, func(client test.TestingClient) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		ctx = metadata.NewOutgoingContext(ctx, metadata.Join(routingMeta, payloadMD))
		stream, err := client.StreamingRequestResponse(ctx)
		require.NoError(t, err)
		err = stream.Send(payloadReq)
		require.NoError(t, err)
		err = stream.CloseSend()
		require.NoError(t, err)
		response, err := stream.Recv()
		require.NoError(t, err)
		assert.Empty(t, cmp.Diff(response, streamResponse, protocmp.Transform()))
		_, err = stream.Recv()
		assert.Equal(t, io.EOF, err)
		verifyHeaderAndTrailer(t, stream, responseMD, trailersMD)
	})
}

func TestRouter_StreamImmediateError(t *testing.T) {
	ctrl := gomock.NewController(t)
	routingMeta := routingMetadata()
	statusWithDetails, err := status.New(codes.InvalidArgument, "some expected error").
		WithDetails(&test.Request{S1: "some details of the error"})
	require.NoError(t, err)
	tun := NewMockTunnel(ctrl)
	tun.EXPECT().
		ForwardStream(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		Return(statusWithDetails.Err())
	runRouterTest(t, tun, func(client test.TestingClient) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		ctx = metadata.NewOutgoingContext(ctx, routingMeta)
		stream, err := client.StreamingRequestResponse(ctx)
		require.NoError(t, err)
		_, err = stream.Recv()
		require.Error(t, err)
		receivedStatus := status.Convert(err).Proto()
		assert.Empty(t, cmp.Diff(receivedStatus, statusWithDetails.Proto(), protocmp.Transform()))
	})
}

func TestRouter_StreamErrorAfterHeader(t *testing.T) {
	ctrl := gomock.NewController(t)
	routingMeta := routingMetadata()
	payloadMD, responseMD, trailersMD := meta()
	statusWithDetails, err := status.New(codes.InvalidArgument, "some expected error").
		WithDetails(&test.Request{S1: "some details of the error"})
	require.NoError(t, err)
	tun := NewMockTunnel(ctrl)
	tun.EXPECT().
		ForwardStream(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		DoAndReturn(func(log *zap.Logger, rpcAPI modshared.RPCAPI, incomingStream grpc.ServerStream, cb DataCallback) error {
			verifyMeta(t, incomingStream, routingMeta, payloadMD)
			assert.NoError(t, cb.Header(grpctool.MetaToValuesMap(responseMD)))
			assert.NoError(t, cb.Trailer(grpctool.MetaToValuesMap(trailersMD)))
			return statusWithDetails.Err()
		})
	runRouterTest(t, tun, func(client test.TestingClient) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		ctx = metadata.NewOutgoingContext(ctx, metadata.Join(routingMeta, payloadMD))
		stream, err := client.StreamingRequestResponse(ctx)
		require.NoError(t, err)
		_, err = stream.Recv()
		require.Error(t, err)
		receivedStatus := status.Convert(err).Proto()
		assert.Empty(t, cmp.Diff(receivedStatus, statusWithDetails.Proto(), protocmp.Transform()))
		verifyHeaderAndTrailer(t, stream, responseMD, trailersMD)
	})
}

func TestRouter_StreamVisitorErrorAfterErrorMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	routingMeta := routingMetadata()
	payloadMD, responseMD, trailersMD := meta()
	statusWithDetails, err := status.New(codes.InvalidArgument, "some expected error").
		WithDetails(&test.Request{S1: "some details of the error"})
	require.NoError(t, err)
	tun := NewMockTunnel(ctrl)
	tun.EXPECT().
		ForwardStream(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		DoAndReturn(func(log *zap.Logger, rpcAPI modshared.RPCAPI, incomingStream grpc.ServerStream, cb DataCallback) error {
			verifyMeta(t, incomingStream, routingMeta, payloadMD)
			assert.NoError(t, cb.Header(grpctool.MetaToValuesMap(responseMD)))
			assert.NoError(t, cb.Trailer(grpctool.MetaToValuesMap(trailersMD)))
			assert.NoError(t, cb.Error(statusWithDetails.Proto()))
			return status.Error(codes.Unavailable, "expected return error")
		})
	runRouterTest(t, tun, func(client test.TestingClient) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		ctx = metadata.NewOutgoingContext(ctx, metadata.Join(routingMeta, payloadMD))
		stream, err := client.StreamingRequestResponse(ctx)
		require.NoError(t, err)
		_, err = stream.Recv()
		require.EqualError(t, err, "rpc error: code = Unavailable desc = expected return error")
		verifyHeaderAndTrailer(t, stream, responseMD, trailersMD)
	})
}

func TestRouter_FindTunnelTimeout(t *testing.T) {
	ctrl := gomock.NewController(t)
	rep := mock_tool.NewMockErrReporter(ctrl)
	log := zaptest.NewLogger(t)
	querier := NewMockPollingGatewayURLQuerier(ctrl)
	internalServerListener := grpctool.NewDialListener()
	defer internalServerListener.Close()
	privateAPIServerListener, err := net.Listen("tcp", "localhost:0")
	require.NoError(t, err)
	defer privateAPIServerListener.Close()

	gomock.InOrder(
		querier.EXPECT().
			CachedGatewayURLs(testhelpers.AgentID),
		querier.EXPECT().
			PollGatewayURLs(gomock.Any(), testhelpers.AgentID, gomock.Any()).
			Do(func(ctx context.Context, agentID int64, cb PollGatewayURLsCallback) {
				<-ctx.Done()
			}),
	)
	factory := func(ctx context.Context, fullMethodName string) modshared.RPCAPI {
		rpcAPI := mock_modshared.NewMockRPCAPI(ctrl)
		rpcAPI.EXPECT().
			Log().
			Return(log).
			AnyTimes()
		rpcAPI.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
			AnyTimes()
		return rpcAPI
	}

	internalServer := grpc.NewServer(
		grpc.StatsHandler(grpctool.NewServerMaxConnAgeStatsHandler(context.Background(), 0)),
		grpc.ChainStreamInterceptor(
			modshared.StreamRPCAPIInterceptor(factory),
		),
		grpc.ChainUnaryInterceptor(
			modshared.UnaryRPCAPIInterceptor(factory),
		),
		grpc.ForceServerCodec(grpctool.RawCodec{}),
	)
	privateAPIServer := grpc.NewServer(
		grpc.StatsHandler(grpctool.NewServerMaxConnAgeStatsHandler(context.Background(), 0)),
		grpc.ChainStreamInterceptor(
			modshared.StreamRPCAPIInterceptor(factory),
		),
		grpc.ChainUnaryInterceptor(
			modshared.UnaryRPCAPIInterceptor(factory),
		),
		grpc.ForceServerCodec(grpctool.RawCodecWithProtoFallback{}),
	)
	tm := metricnoop.NewMeterProvider().Meter("test")
	routingDuration, err := tm.Float64Histogram(routingDurationMetricName)
	require.NoError(t, err)
	routingTimeout, err := tm.Int64Counter(routingTimeoutMetricName)
	require.NoError(t, err)

	kasPool := grpctool.NewPool(log, rep,
		credentials.NewTLS(tlstool.DefaultClientTLSConfig()),
		grpc.WithContextDialer(func(ctx context.Context, addr string) (net.Conn, error) {
			<-ctx.Done()
			return nil, ctx.Err()
		}),
	)
	defer func() {
		assert.NoError(t, kasPool.Close())
	}()
	rp := NewMockRouterPlugin(ctrl)
	rp.EXPECT().
		GatewayFinderForStream(gomock.Any(), gomock.Any()).
		DoAndReturn(func(stream grpc.ServerStream, rpcAPI modshared.RPCAPI) (GatewayFinder, *zap.Logger, int64, error) {
			ctx := stream.Context()
			l := rpcAPI.Log()
			md, _ := metadata.FromIncomingContext(ctx)
			gf := NewGatewayFinder(
				l,
				kasPool,
				querier,
				rpcAPI,
				grpc.ServerTransportStreamFromContext(ctx).Method(),
				"grpc://"+privateAPIServerListener.Addr().String(),
				testhelpers.AgentID,
				metadata.NewOutgoingContext(ctx, md),
				testhelpers.NewPollConfig(time.Minute),
				20*time.Millisecond,
			)
			return gf, l, testhelpers.AgentID, nil
		})

	r := &Router{
		plugin:            rp,
		internalServer:    internalServer,
		privateAPIServer:  privateAPIServer,
		tracer:            tracenoop.NewTracerProvider().Tracer(routerTracerName),
		routingDuration:   routingDuration,
		routingTimeout:    routingTimeout,
		tunnelFindTimeout: 100 * time.Millisecond,
	}
	r.RegisterTunclientAPI(&test.Testing_ServiceDesc)
	var wg wait.Group
	defer wg.Wait()
	defer internalServer.GracefulStop()
	defer privateAPIServer.GracefulStop()
	wg.Start(func() {
		assert.NoError(t, internalServer.Serve(internalServerListener))
	})
	wg.Start(func() {
		assert.NoError(t, privateAPIServer.Serve(privateAPIServerListener))
	})
	internalServerConn, err := grpc.DialContext(context.Background(), "passthrough:pipe",
		grpc.WithContextDialer(internalServerListener.DialContext),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainStreamInterceptor(
			grpctool.StreamClientValidatingInterceptor,
		),
		grpc.WithChainUnaryInterceptor(
			grpctool.UnaryClientValidatingInterceptor,
		),
	)
	require.NoError(t, err)
	defer internalServerConn.Close()
	client := test.NewTestingClient(internalServerConn)
	routingMeta := routingMetadata()
	ctx := metadata.NewOutgoingContext(context.Background(), routingMeta)
	_, err = client.RequestResponse(ctx, &test.Request{})
	assert.EqualError(t, err, "rpc error: code = DeadlineExceeded desc = agent connection not found. Is agent up to date and connected?")
}

func meta() (metadata.MD, metadata.MD, metadata.MD) {
	payloadMD := metadata.Pairs("key1", "value1")
	responseMD := metadata.Pairs("key2", "value2")
	trailersMD := metadata.Pairs("key3", "value3")
	return payloadMD, responseMD, trailersMD
}

func verifyHeaderAndTrailer(t *testing.T, stream grpc.ClientStream, responseMD, trailersMD metadata.MD) {
	headerResp, err := stream.Header()
	require.NoError(t, err)
	mdContains(t, responseMD, headerResp)
	mdContains(t, trailersMD, stream.Trailer())
}

func forwardStream(t *testing.T, routingMetadata, payloadMD metadata.MD, payloadReq *test.Request, response *test.Response, responseMD, trailersMD metadata.MD) func(*zap.Logger, modshared.RPCAPI, grpc.ServerStream, DataCallback) error {
	return func(log *zap.Logger, rpcAPI modshared.RPCAPI, incomingStream grpc.ServerStream, cb DataCallback) error {
		verifyMeta(t, incomingStream, routingMetadata, payloadMD)
		var req test.Request
		err := incomingStream.RecvMsg(&req)
		assert.NoError(t, err)
		assert.Empty(t, cmp.Diff(payloadReq, &req, protocmp.Transform()))
		data, err := proto.Marshal(response)
		assert.NoError(t, err)
		assert.NoError(t, cb.Header(grpctool.MetaToValuesMap(responseMD)))
		assert.NoError(t, cb.Message(data))
		assert.NoError(t, cb.Trailer(grpctool.MetaToValuesMap(trailersMD)))
		return nil
	}
}

func verifyMeta(t *testing.T, incomingStream grpc.ServerStream, routingMetadata, payloadMD metadata.MD) {
	md, _ := metadata.FromIncomingContext(incomingStream.Context())
	for k := range routingMetadata { // no routing metadata is passed to the agent
		assert.NotContains(t, md, k)
	}
	mdContains(t, payloadMD, md)
}

func mdContains(t *testing.T, expectedMd metadata.MD, actualMd metadata.MD) {
	for k, v := range expectedMd {
		assert.Equalf(t, v, actualMd[k], "key: %s", k)
	}
}

// test:client(default codec) --> kas:internal server(raw codec) --> router_kas handler -->
// client from kas_pool(raw with fallback codec) --> kas:private server(raw with fallback codec) -->
// router_agent handler --> tunnel finder --> tunnel.ForwardStream()
func runRouterTest(t *testing.T, tunnel *MockTunnel, runTest func(client test.TestingClient)) {
	ctrl := gomock.NewController(t)
	rep := mock_tool.NewMockErrReporter(ctrl)
	log := zaptest.NewLogger(t)
	querier := NewMockPollingGatewayURLQuerier(ctrl)
	fh := NewMockFindHandle(ctrl)
	internalServerListener := grpctool.NewDialListener()
	defer internalServerListener.Close()
	privateAPIServerListener, err := net.Listen("tcp", "localhost:0")
	require.NoError(t, err)
	defer privateAPIServerListener.Close()

	querier.EXPECT().
		CachedGatewayURLs(testhelpers.AgentID)
	factory := func(ctx context.Context, fullMethodName string) modshared.RPCAPI {
		rpcAPI := mock_modshared.NewMockRPCAPI(ctrl)
		rpcAPI.EXPECT().
			Log().
			Return(log).
			AnyTimes()
		rpcAPI.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
			AnyTimes()
		return rpcAPI
	}

	internalServer := grpc.NewServer(
		grpc.StatsHandler(grpctool.NewServerMaxConnAgeStatsHandler(context.Background(), 0)),
		grpc.ChainStreamInterceptor(
			modshared.StreamRPCAPIInterceptor(factory),
		),
		grpc.ChainUnaryInterceptor(
			modshared.UnaryRPCAPIInterceptor(factory),
		),
		grpc.ForceServerCodec(grpctool.RawCodec{}),
	)
	privateAPIServer := grpc.NewServer(
		grpc.StatsHandler(grpctool.NewServerMaxConnAgeStatsHandler(context.Background(), 0)),
		grpc.ChainStreamInterceptor(
			modshared.StreamRPCAPIInterceptor(factory),
		),
		grpc.ChainUnaryInterceptor(
			modshared.UnaryRPCAPIInterceptor(factory),
		),
		grpc.ForceServerCodec(grpctool.RawCodecWithProtoFallback{}),
	)
	tm := metricnoop.NewMeterProvider().Meter("test")
	kasRoutingDuration, err := tm.Float64Histogram(routingDurationMetricName)
	require.NoError(t, err)
	kasRoutingTimeout, err := tm.Int64Counter(routingTimeoutMetricName)
	require.NoError(t, err)

	kasPool := grpctool.NewPool(log, rep,
		credentials.NewTLS(tlstool.DefaultClientTLSConfig()),
	)
	defer func() {
		assert.NoError(t, kasPool.Close())
	}()
	rp := NewMockRouterPlugin(ctrl)
	rp.EXPECT().
		GatewayFinderForStream(gomock.Any(), gomock.Any()).
		DoAndReturn(func(stream grpc.ServerStream, rpcAPI modshared.RPCAPI) (GatewayFinder, *zap.Logger, int64, error) {
			ctx := stream.Context()
			l := rpcAPI.Log()
			md, _ := metadata.FromIncomingContext(ctx)
			gf := NewGatewayFinder(
				l,
				kasPool,
				querier,
				rpcAPI,
				grpc.ServerTransportStreamFromContext(ctx).Method(),
				"grpc://"+privateAPIServerListener.Addr().String(),
				testhelpers.AgentID,
				metadata.NewOutgoingContext(ctx, md),
				testhelpers.NewPollConfig(time.Minute),
				// We don't want any nondeterministic polls to other KAS
				5*time.Second,
			)
			return gf, l, testhelpers.AgentID, nil
		})
	gomock.InOrder(
		rp.EXPECT().
			FindTunnel(gomock.Any(), gomock.Any()).
			Return(true, log, fh, nil),
		fh.EXPECT().
			Get(gomock.Any()).
			Return(tunnel, nil),
		tunnel.EXPECT().Done(gomock.Any()),
		fh.EXPECT().Done(gomock.Any()),
	)

	r := &Router{
		plugin:            rp,
		internalServer:    internalServer,
		privateAPIServer:  privateAPIServer,
		tracer:            tracenoop.NewTracerProvider().Tracer(routerTracerName),
		routingDuration:   kasRoutingDuration,
		routingTimeout:    kasRoutingTimeout,
		tunnelFindTimeout: 20 * time.Second,
	}
	r.RegisterTunclientAPI(&test.Testing_ServiceDesc)
	var wg wait.Group
	defer wg.Wait()
	defer internalServer.GracefulStop()
	defer privateAPIServer.GracefulStop()
	wg.Start(func() {
		assert.NoError(t, internalServer.Serve(internalServerListener))
	})
	wg.Start(func() {
		assert.NoError(t, privateAPIServer.Serve(privateAPIServerListener))
	})
	internalServerConn, err := grpc.DialContext(context.Background(), "passthrough:pipe",
		grpc.WithContextDialer(internalServerListener.DialContext),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainStreamInterceptor(
			grpctool.StreamClientValidatingInterceptor,
		),
		grpc.WithChainUnaryInterceptor(
			grpctool.UnaryClientValidatingInterceptor,
		),
	)
	require.NoError(t, err)
	defer internalServerConn.Close()
	client := test.NewTestingClient(internalServerConn)
	runTest(client)
}

func routingMetadata() metadata.MD {
	return metadata.Pairs(RoutingHopPrefix+"test", "some data")
}
