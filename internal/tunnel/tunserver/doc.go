package tunserver

//go:generate mockgen.sh -self_package "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/tunserver" -destination "mock_for_test.go" -package "tunserver" "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/tunserver" "DataCallback,Tunnel,FindHandle,PollingGatewayURLQuerier,RouterPlugin"
