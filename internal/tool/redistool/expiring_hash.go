package redistool

import (
	"context"
	"errors"
	"strconv"
	"time"
	"unsafe"

	"github.com/redis/rueidis"
	"go.opentelemetry.io/otel/attribute"
	otelmetric "go.opentelemetry.io/otel/metric"
	"google.golang.org/protobuf/proto"
)

const (
	maxKeyGCAttempts        = 2
	gcDeletedKeysMetricName = "redis_expiring_hash_gc_deleted_keys_count"
	gcConflictMetricName    = "redis_expiring_hash_gc_conflict"
)

// KeyToRedisKey is used to convert typed key (key1 or key2) into a string.
// HSET key1 key2 value.
type KeyToRedisKey[K any] func(key K) string

// ExpiringHash represents a two-level hash: key K1 -> hashKey K2 -> value []byte.
// key identifies the hash; hashKey identifies the key in the hash; value is the value for the hashKey.
// It is not safe for concurrent use.
type ExpiringHash[K1 any, K2 any] interface {
	// Set sets the key -> hashKey -> value. The mapping is stored in RAM and in the backing store.
	// Use Refresh to refresh the value in the backing store.
	Set(ctx context.Context, key K1, hashKey K2, value []byte) error
	// SetEX sets the key -> hashKey -> value. The value is stored in the backing store only (unlike Set).
	// Refresh does not refresh this value in the backing store. Use this method to re-set (i.e. refresh) the value
	// in the backing store.
	// Safe for concurrent use.
	SetEX(ctx context.Context, key K1, hashKey K2, value []byte, expiresAt time.Time) error
	Unset(ctx context.Context, key K1, hashKey K2) error
	// Forget only removes the item from the in-memory map.
	Forget(key K1, hashKey K2)
	// ScanAndGC iterates key-value pairs for key. It removes any expired entries it finds.
	// Safe for concurrent use.
	ScanAndGC(ctx context.Context, key K1, cb ScanAndGCCallback) (int /* keysDeleted */, error)
	// Len returns number of key-value mappings in the hash identified by key.
	Len(ctx context.Context, key K1) (int64, error)
	// GC returns a function that iterates all relevant stored data and deletes expired entries.
	// The returned function can be called concurrently as it does not interfere with the hash's operation.
	// The function returns number of deleted Redis (hash) keys, including when an error occurred.
	// It only inspects/GCs hashes where it has entries. Other concurrent clients GC same and/or other corresponding hashes.
	// Hashes that don't have a corresponding client (e.g. because it crashed) will expire because of TTL on the hash key.
	// GC only needs to be used if Len() is used. Otherwise expired entries will be found and deleted by Scan().
	GC() func(context.Context) (int /* keysDeleted */, error)
	// GCFor returns a function that iterates the hash for the given keys and deletes expired entries.
	// GCFor is useful when executing GC for specific keys.
	GCFor(keys []K1) func(context.Context) (int /* keysDeleted */, error)
	// Clear clears all data in this hash and deletes it from the backing store.
	Clear(context.Context) (int, error)
	// Refresh refreshes data in the backing store to prevent it from expiring.
	Refresh(ctx context.Context, nextRefresh time.Time) error
}

type RedisExpiringHash[K1 comparable, K2 comparable] struct {
	client           rueidis.Client
	key1ToRedisKey   KeyToRedisKey[K1]
	key2ToRedisKey   KeyToRedisKey[K2]
	ttl              time.Duration
	api              *RedisExpiringHashAPI[K1, K2]
	data             map[K1]map[K2]*ExpiringValue // key -> hash key -> value
	gcCounter        otelmetric.Int64Counter
	gcConflict       otelmetric.Int64Counter
	metricAttributes attribute.Set
	transactionalGC  bool
}

func NewRedisExpiringHash[K1 comparable, K2 comparable](name string, client rueidis.Client, key1ToRedisKey KeyToRedisKey[K1],
	key2ToRedisKey KeyToRedisKey[K2], ttl time.Duration, m otelmetric.Meter, transactionalGC bool) (*RedisExpiringHash[K1, K2], error) {
	api, err := NewRedisExpiringHashAPI[K1, K2](name, client, key1ToRedisKey, key2ToRedisKey, m)
	if err != nil {
		return nil, err
	}

	gcCounter, err := m.Int64Counter(
		gcDeletedKeysMetricName,
		otelmetric.WithDescription("Number of keys that have been garbage collected in a single pass"),
	)
	if err != nil {
		return nil, err
	}
	gcConflict, err := m.Int64Counter(
		gcConflictMetricName,
		otelmetric.WithDescription("Number of times garbage collection was aborted due to a concurrent hash mutation"),
	)
	if err != nil {
		return nil, err
	}

	return &RedisExpiringHash[K1, K2]{
		client:           client,
		key1ToRedisKey:   key1ToRedisKey,
		key2ToRedisKey:   key2ToRedisKey,
		ttl:              ttl,
		api:              api,
		data:             make(map[K1]map[K2]*ExpiringValue),
		gcCounter:        gcCounter,
		gcConflict:       gcConflict,
		metricAttributes: attribute.NewSet(expiringHashNameKey.String(name)),
		transactionalGC:  transactionalGC,
	}, nil
}

func (h *RedisExpiringHash[K1, K2]) Set(ctx context.Context, key K1, hashKey K2, value []byte) error {
	ev := &ExpiringValue{
		ExpiresAt: time.Now().Add(h.ttl).Unix(),
		Value:     value,
	}
	h.setData(key, hashKey, ev)

	b := h.api.SetBuilder()
	b.Set(key, h.ttl, BuilderKV[K2]{
		HashKey: hashKey,
		Value:   ev,
	})
	return b.Do(ctx)
}

func (h *RedisExpiringHash[K1, K2]) SetEX(ctx context.Context, key K1, hashKey K2, value []byte, expiresAt time.Time) error {
	b := h.api.SetBuilder()
	b.Set(key, h.ttl, BuilderKV[K2]{
		HashKey: hashKey,
		Value: &ExpiringValue{
			ExpiresAt: expiresAt.Unix(),
			Value:     value,
		},
	})
	return b.Do(ctx)
}

func (h *RedisExpiringHash[K1, K2]) Unset(ctx context.Context, key K1, hashKey K2) error {
	h.unsetData(key, hashKey)
	return h.api.Unset(ctx, key, hashKey)
}

func (h *RedisExpiringHash[K1, K2]) Forget(key K1, hashKey K2) {
	h.unsetData(key, hashKey)
}

func (h *RedisExpiringHash[K1, K2]) Len(ctx context.Context, key K1) (size int64, retErr error) {
	hlenCmd := h.client.B().Hlen().Key(h.key1ToRedisKey(key)).Build()
	return h.client.Do(ctx, hlenCmd).AsInt64()
}

func (h *RedisExpiringHash[K1, K2]) ScanAndGC(ctx context.Context, key K1, cb ScanAndGCCallback) (int /* keysDeleted */, error) {
	return h.api.ScanAndGC(ctx, key, cb)
}

type scanCb func(k, v string) (bool /*done*/, error)

func scan(ctx context.Context, redisKey string, c rueidis.CoreClient, cb scanCb) error {
	// Scan keys of a hash. See https://redis.io/commands/scan
	var se rueidis.ScanEntry
	var err error
	for more := true; more; more = se.Cursor != 0 {
		hscanCmd := c.B().Hscan().Key(redisKey).Cursor(se.Cursor).Build()
		se, err = c.Do(ctx, hscanCmd).AsScanEntry()
		if err != nil {
			return err
		}
		if len(se.Elements)%2 != 0 {
			// This shouldn't happen
			return errors.New("invalid Redis reply")
		}
		for i := 0; i < len(se.Elements); i += 2 {
			k := se.Elements[i]
			v := se.Elements[i+1]
			done, err := cb(k, v)
			if err != nil || done {
				return err
			}
		}
	}
	return nil
}

func (h *RedisExpiringHash[K1, K2]) GC() func(context.Context) (int /* keysDeleted */, error) {
	// Copy keys for safe concurrent access.
	keys := make([]K1, 0, len(h.data))
	for key := range h.data {
		keys = append(keys, key)
	}
	return h.GCFor(keys)
}

func (h *RedisExpiringHash[K1, K2]) GCFor(keys []K1) func(context.Context) (int /* keysDeleted */, error) {
	return func(ctx context.Context) (deletedKeys int, retErr error) {
		defer func() {
			h.gcCounter.Add(context.Background(), int64(deletedKeys), otelmetric.WithAttributeSet(h.metricAttributes))
		}()

		if h.transactionalGC {
			return h.gcForTransactional(ctx, keys)
		} else {
			return h.gcForNonTransactional(ctx, keys)
		}
	}
}

func (h *RedisExpiringHash[K1, K2]) gcForNonTransactional(ctx context.Context, keys []K1) (int /* keysDeleted */, error) {
	var deletedKeys int
	for _, key := range keys {
		deleted, err := h.gcHashNonTransactional(ctx, key)
		deletedKeys += deleted
		if err != nil {
			return deletedKeys, err
		}
	}
	return deletedKeys, nil
}

// gcHashNonTransactional iterates a hash and removes all expired values.
// It assumes that values are marshaled ExpiringValue.
func (h *RedisExpiringHash[K1, K2]) gcHashNonTransactional(ctx context.Context, key K1) (int /* keysDeleted */, error) {
	redisKey := h.key1ToRedisKey(key)
	keysToDelete, errs := h.getKeysToGC(ctx, redisKey, h.client)
	if len(keysToDelete) == 0 {
		return 0, errors.Join(errs...)
	}
	delCmd := h.client.B().Hdel().Key(redisKey).Field(keysToDelete...).Build()
	err := h.client.Do(ctx, delCmd).Error()
	if err != nil {
		errs = append(errs, err)
	}
	return len(keysToDelete), errors.Join(errs...)
}

func (h *RedisExpiringHash[K1, K2]) gcForTransactional(ctx context.Context, keys []K1) (int /* keysDeleted */, error) {
	var deletedKeys int
	client, cancel := h.client.Dedicate()
	defer cancel()
	for _, key := range keys {
		deleted, err := h.gcHashTransactional(ctx, key, client)
		deletedKeys += deleted
		switch err { //nolint:errorlint
		case nil, attemptsExceeded:
			// Try to GC next key on conflicts
		default:
			return deletedKeys, err
		}
	}
	return deletedKeys, nil
}

// gcHashTransactional iterates a hash and removes all expired values.
// It assumes that values are marshaled ExpiringValue.
// Returns attemptsExceeded if maxAttempts attempts ware made but all failed.
func (h *RedisExpiringHash[K1, K2]) gcHashTransactional(ctx context.Context, key K1, c rueidis.DedicatedClient) (int /* keysDeleted */, error) {
	var errs []error
	keysDeleted := 0
	redisKey := h.key1ToRedisKey(key)
	// We don't want to delete a k->v mapping that has just been overwritten by another client. So use a transaction.
	// We don't want to retry too many times to GC to avoid spending too much time on it. Retry once.
	err := transaction(ctx, maxKeyGCAttempts, c, h.gcConflict, h.metricAttributes, func(ctx context.Context) ([]rueidis.Completed, error) {
		var keysToDelete []string
		keysToDelete, errs = h.getKeysToGC(ctx, redisKey, c)
		keysDeleted = len(keysToDelete)
		if keysDeleted == 0 {
			return nil, nil // errs is handled outside of the closure
		}
		return []rueidis.Completed{
			c.B().Hdel().Key(redisKey).Field(keysToDelete...).Build(),
		}, nil
	}, redisKey)
	if err != nil {
		// Propagate attemptsExceeded error and any other errors as is.
		return 0, err
	}
	return keysDeleted, errors.Join(errs...)
}

func (h *RedisExpiringHash[K1, K2]) getKeysToGC(ctx context.Context, redisKey string, c rueidis.CoreClient) ([]string, []error) {
	var errs []error
	var keysToDelete []string
	now := time.Now().Unix()
	err := scan(ctx, redisKey, c, func(k, v string) (bool /*done*/, error) {
		var msg ExpiringValueTimestamp
		// Avoid creating a temporary copy
		vBytes := unsafe.Slice(unsafe.StringData(v), len(v)) //nolint: gosec
		err := proto.UnmarshalOptions{
			DiscardUnknown: true, // We know there is one more field, but we don't need it
		}.Unmarshal(vBytes, &msg)
		if err != nil {
			errs = append(errs, err)
			return false, nil
		}

		if msg.ExpiresAt < now {
			keysToDelete = append(keysToDelete, k)
		}
		return false, nil
	})
	if err != nil {
		errs = append(errs, err)
	}
	return keysToDelete, errs
}

func (h *RedisExpiringHash[K1, K2]) Clear(ctx context.Context) (int, error) {
	var toDel []string
	keysDeleted := 0
	cmds := make([]rueidis.Completed, 0, len(h.data))
	for k1, m := range h.data {
		toDel = toDel[:0] // reuse backing array, but reset length
		for k2 := range m {
			toDel = append(toDel, h.key2ToRedisKey(k2))
		}
		cmds = append(cmds, h.client.B().Hdel().Key(h.key1ToRedisKey(k1)).Field(toDel...).Build())
		delete(h.data, k1)
		keysDeleted += len(toDel)
	}
	errs := MultiErrors(h.client.DoMulti(ctx, cmds...))
	return keysDeleted, errors.Join(errs...)
}

func (h *RedisExpiringHash[K1, K2]) Refresh(ctx context.Context, nextRefresh time.Time) error {
	expiresAt := time.Now().Add(h.ttl).Unix()
	nextRefreshUnix := nextRefresh.Unix()
	b := h.api.SetBuilder()
	var kvs []BuilderKV[K2]
	for key, hashData := range h.data {
		kvs = kvs[:0] // reuse backing array, but reset length
		for hashKey, value := range hashData {
			if value.ExpiresAt > nextRefreshUnix {
				// Expires after next refresh. Will be refreshed later, no need to refresh now.
				continue
			}
			value.ExpiresAt = expiresAt
			kvs = append(kvs, BuilderKV[K2]{
				HashKey: hashKey,
				Value:   value,
			})
		}
		b.Set(key, h.ttl, kvs...)
	}
	return b.Do(ctx)
}

func (h *RedisExpiringHash[K1, K2]) setData(key K1, hashKey K2, value *ExpiringValue) {
	nm := h.data[key]
	if nm == nil {
		nm = make(map[K2]*ExpiringValue, 1)
		h.data[key] = nm
	}
	nm[hashKey] = value
}

func (h *RedisExpiringHash[K1, K2]) unsetData(key K1, hashKey K2) {
	nm := h.data[key]
	delete(nm, hashKey)
	if len(nm) == 0 {
		delete(h.data, key)
	}
}

func PrefixedInt64Key(prefix string, key int64) string {
	return prefix + strconv.FormatInt(key, 32)
}
