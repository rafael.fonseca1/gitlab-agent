// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool (interfaces: InboundGRPCToOutboundHTTPStream,PoolConn,PoolInterface,ServerErrorReporter)
//
// Generated by this command:
//
//	mockgen -typed -destination grpctool.go -package mock_rpc gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool InboundGRPCToOutboundHTTPStream,PoolConn,PoolInterface,ServerErrorReporter
//

// Package mock_rpc is a generated GoMock package.
package mock_rpc

import (
	context "context"
	reflect "reflect"

	grpctool "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool"
	gomock "go.uber.org/mock/gomock"
	grpc "google.golang.org/grpc"
	metadata "google.golang.org/grpc/metadata"
)

// MockInboundGRPCToOutboundHTTPStream is a mock of InboundGRPCToOutboundHTTPStream interface.
type MockInboundGRPCToOutboundHTTPStream struct {
	ctrl     *gomock.Controller
	recorder *MockInboundGRPCToOutboundHTTPStreamMockRecorder
}

// MockInboundGRPCToOutboundHTTPStreamMockRecorder is the mock recorder for MockInboundGRPCToOutboundHTTPStream.
type MockInboundGRPCToOutboundHTTPStreamMockRecorder struct {
	mock *MockInboundGRPCToOutboundHTTPStream
}

// NewMockInboundGRPCToOutboundHTTPStream creates a new mock instance.
func NewMockInboundGRPCToOutboundHTTPStream(ctrl *gomock.Controller) *MockInboundGRPCToOutboundHTTPStream {
	mock := &MockInboundGRPCToOutboundHTTPStream{ctrl: ctrl}
	mock.recorder = &MockInboundGRPCToOutboundHTTPStreamMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockInboundGRPCToOutboundHTTPStream) EXPECT() *MockInboundGRPCToOutboundHTTPStreamMockRecorder {
	return m.recorder
}

// Context mocks base method.
func (m *MockInboundGRPCToOutboundHTTPStream) Context() context.Context {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Context")
	ret0, _ := ret[0].(context.Context)
	return ret0
}

// Context indicates an expected call of Context.
func (mr *MockInboundGRPCToOutboundHTTPStreamMockRecorder) Context() *MockInboundGRPCToOutboundHTTPStreamContextCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Context", reflect.TypeOf((*MockInboundGRPCToOutboundHTTPStream)(nil).Context))
	return &MockInboundGRPCToOutboundHTTPStreamContextCall{Call: call}
}

// MockInboundGRPCToOutboundHTTPStreamContextCall wrap *gomock.Call
type MockInboundGRPCToOutboundHTTPStreamContextCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockInboundGRPCToOutboundHTTPStreamContextCall) Return(arg0 context.Context) *MockInboundGRPCToOutboundHTTPStreamContextCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockInboundGRPCToOutboundHTTPStreamContextCall) Do(f func() context.Context) *MockInboundGRPCToOutboundHTTPStreamContextCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockInboundGRPCToOutboundHTTPStreamContextCall) DoAndReturn(f func() context.Context) *MockInboundGRPCToOutboundHTTPStreamContextCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// RecvMsg mocks base method.
func (m *MockInboundGRPCToOutboundHTTPStream) RecvMsg(arg0 any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RecvMsg", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// RecvMsg indicates an expected call of RecvMsg.
func (mr *MockInboundGRPCToOutboundHTTPStreamMockRecorder) RecvMsg(arg0 any) *MockInboundGRPCToOutboundHTTPStreamRecvMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RecvMsg", reflect.TypeOf((*MockInboundGRPCToOutboundHTTPStream)(nil).RecvMsg), arg0)
	return &MockInboundGRPCToOutboundHTTPStreamRecvMsgCall{Call: call}
}

// MockInboundGRPCToOutboundHTTPStreamRecvMsgCall wrap *gomock.Call
type MockInboundGRPCToOutboundHTTPStreamRecvMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockInboundGRPCToOutboundHTTPStreamRecvMsgCall) Return(arg0 error) *MockInboundGRPCToOutboundHTTPStreamRecvMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockInboundGRPCToOutboundHTTPStreamRecvMsgCall) Do(f func(any) error) *MockInboundGRPCToOutboundHTTPStreamRecvMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockInboundGRPCToOutboundHTTPStreamRecvMsgCall) DoAndReturn(f func(any) error) *MockInboundGRPCToOutboundHTTPStreamRecvMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Send mocks base method.
func (m *MockInboundGRPCToOutboundHTTPStream) Send(arg0 *grpctool.HttpResponse) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Send", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// Send indicates an expected call of Send.
func (mr *MockInboundGRPCToOutboundHTTPStreamMockRecorder) Send(arg0 any) *MockInboundGRPCToOutboundHTTPStreamSendCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Send", reflect.TypeOf((*MockInboundGRPCToOutboundHTTPStream)(nil).Send), arg0)
	return &MockInboundGRPCToOutboundHTTPStreamSendCall{Call: call}
}

// MockInboundGRPCToOutboundHTTPStreamSendCall wrap *gomock.Call
type MockInboundGRPCToOutboundHTTPStreamSendCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockInboundGRPCToOutboundHTTPStreamSendCall) Return(arg0 error) *MockInboundGRPCToOutboundHTTPStreamSendCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockInboundGRPCToOutboundHTTPStreamSendCall) Do(f func(*grpctool.HttpResponse) error) *MockInboundGRPCToOutboundHTTPStreamSendCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockInboundGRPCToOutboundHTTPStreamSendCall) DoAndReturn(f func(*grpctool.HttpResponse) error) *MockInboundGRPCToOutboundHTTPStreamSendCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendHeader mocks base method.
func (m *MockInboundGRPCToOutboundHTTPStream) SendHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SendHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendHeader indicates an expected call of SendHeader.
func (mr *MockInboundGRPCToOutboundHTTPStreamMockRecorder) SendHeader(arg0 any) *MockInboundGRPCToOutboundHTTPStreamSendHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendHeader", reflect.TypeOf((*MockInboundGRPCToOutboundHTTPStream)(nil).SendHeader), arg0)
	return &MockInboundGRPCToOutboundHTTPStreamSendHeaderCall{Call: call}
}

// MockInboundGRPCToOutboundHTTPStreamSendHeaderCall wrap *gomock.Call
type MockInboundGRPCToOutboundHTTPStreamSendHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockInboundGRPCToOutboundHTTPStreamSendHeaderCall) Return(arg0 error) *MockInboundGRPCToOutboundHTTPStreamSendHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockInboundGRPCToOutboundHTTPStreamSendHeaderCall) Do(f func(metadata.MD) error) *MockInboundGRPCToOutboundHTTPStreamSendHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockInboundGRPCToOutboundHTTPStreamSendHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockInboundGRPCToOutboundHTTPStreamSendHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendMsg mocks base method.
func (m *MockInboundGRPCToOutboundHTTPStream) SendMsg(arg0 any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SendMsg", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendMsg indicates an expected call of SendMsg.
func (mr *MockInboundGRPCToOutboundHTTPStreamMockRecorder) SendMsg(arg0 any) *MockInboundGRPCToOutboundHTTPStreamSendMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendMsg", reflect.TypeOf((*MockInboundGRPCToOutboundHTTPStream)(nil).SendMsg), arg0)
	return &MockInboundGRPCToOutboundHTTPStreamSendMsgCall{Call: call}
}

// MockInboundGRPCToOutboundHTTPStreamSendMsgCall wrap *gomock.Call
type MockInboundGRPCToOutboundHTTPStreamSendMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockInboundGRPCToOutboundHTTPStreamSendMsgCall) Return(arg0 error) *MockInboundGRPCToOutboundHTTPStreamSendMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockInboundGRPCToOutboundHTTPStreamSendMsgCall) Do(f func(any) error) *MockInboundGRPCToOutboundHTTPStreamSendMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockInboundGRPCToOutboundHTTPStreamSendMsgCall) DoAndReturn(f func(any) error) *MockInboundGRPCToOutboundHTTPStreamSendMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetHeader mocks base method.
func (m *MockInboundGRPCToOutboundHTTPStream) SetHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetHeader indicates an expected call of SetHeader.
func (mr *MockInboundGRPCToOutboundHTTPStreamMockRecorder) SetHeader(arg0 any) *MockInboundGRPCToOutboundHTTPStreamSetHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetHeader", reflect.TypeOf((*MockInboundGRPCToOutboundHTTPStream)(nil).SetHeader), arg0)
	return &MockInboundGRPCToOutboundHTTPStreamSetHeaderCall{Call: call}
}

// MockInboundGRPCToOutboundHTTPStreamSetHeaderCall wrap *gomock.Call
type MockInboundGRPCToOutboundHTTPStreamSetHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockInboundGRPCToOutboundHTTPStreamSetHeaderCall) Return(arg0 error) *MockInboundGRPCToOutboundHTTPStreamSetHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockInboundGRPCToOutboundHTTPStreamSetHeaderCall) Do(f func(metadata.MD) error) *MockInboundGRPCToOutboundHTTPStreamSetHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockInboundGRPCToOutboundHTTPStreamSetHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockInboundGRPCToOutboundHTTPStreamSetHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetTrailer mocks base method.
func (m *MockInboundGRPCToOutboundHTTPStream) SetTrailer(arg0 metadata.MD) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SetTrailer", arg0)
}

// SetTrailer indicates an expected call of SetTrailer.
func (mr *MockInboundGRPCToOutboundHTTPStreamMockRecorder) SetTrailer(arg0 any) *MockInboundGRPCToOutboundHTTPStreamSetTrailerCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetTrailer", reflect.TypeOf((*MockInboundGRPCToOutboundHTTPStream)(nil).SetTrailer), arg0)
	return &MockInboundGRPCToOutboundHTTPStreamSetTrailerCall{Call: call}
}

// MockInboundGRPCToOutboundHTTPStreamSetTrailerCall wrap *gomock.Call
type MockInboundGRPCToOutboundHTTPStreamSetTrailerCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockInboundGRPCToOutboundHTTPStreamSetTrailerCall) Return() *MockInboundGRPCToOutboundHTTPStreamSetTrailerCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockInboundGRPCToOutboundHTTPStreamSetTrailerCall) Do(f func(metadata.MD)) *MockInboundGRPCToOutboundHTTPStreamSetTrailerCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockInboundGRPCToOutboundHTTPStreamSetTrailerCall) DoAndReturn(f func(metadata.MD)) *MockInboundGRPCToOutboundHTTPStreamSetTrailerCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockPoolConn is a mock of PoolConn interface.
type MockPoolConn struct {
	ctrl     *gomock.Controller
	recorder *MockPoolConnMockRecorder
}

// MockPoolConnMockRecorder is the mock recorder for MockPoolConn.
type MockPoolConnMockRecorder struct {
	mock *MockPoolConn
}

// NewMockPoolConn creates a new mock instance.
func NewMockPoolConn(ctrl *gomock.Controller) *MockPoolConn {
	mock := &MockPoolConn{ctrl: ctrl}
	mock.recorder = &MockPoolConnMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockPoolConn) EXPECT() *MockPoolConnMockRecorder {
	return m.recorder
}

// Done mocks base method.
func (m *MockPoolConn) Done() {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Done")
}

// Done indicates an expected call of Done.
func (mr *MockPoolConnMockRecorder) Done() *MockPoolConnDoneCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Done", reflect.TypeOf((*MockPoolConn)(nil).Done))
	return &MockPoolConnDoneCall{Call: call}
}

// MockPoolConnDoneCall wrap *gomock.Call
type MockPoolConnDoneCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockPoolConnDoneCall) Return() *MockPoolConnDoneCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockPoolConnDoneCall) Do(f func()) *MockPoolConnDoneCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockPoolConnDoneCall) DoAndReturn(f func()) *MockPoolConnDoneCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Invoke mocks base method.
func (m *MockPoolConn) Invoke(arg0 context.Context, arg1 string, arg2, arg3 any, arg4 ...grpc.CallOption) error {
	m.ctrl.T.Helper()
	varargs := []any{arg0, arg1, arg2, arg3}
	for _, a := range arg4 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "Invoke", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Invoke indicates an expected call of Invoke.
func (mr *MockPoolConnMockRecorder) Invoke(arg0, arg1, arg2, arg3 any, arg4 ...any) *MockPoolConnInvokeCall {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{arg0, arg1, arg2, arg3}, arg4...)
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Invoke", reflect.TypeOf((*MockPoolConn)(nil).Invoke), varargs...)
	return &MockPoolConnInvokeCall{Call: call}
}

// MockPoolConnInvokeCall wrap *gomock.Call
type MockPoolConnInvokeCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockPoolConnInvokeCall) Return(arg0 error) *MockPoolConnInvokeCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockPoolConnInvokeCall) Do(f func(context.Context, string, any, any, ...grpc.CallOption) error) *MockPoolConnInvokeCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockPoolConnInvokeCall) DoAndReturn(f func(context.Context, string, any, any, ...grpc.CallOption) error) *MockPoolConnInvokeCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// NewStream mocks base method.
func (m *MockPoolConn) NewStream(arg0 context.Context, arg1 *grpc.StreamDesc, arg2 string, arg3 ...grpc.CallOption) (grpc.ClientStream, error) {
	m.ctrl.T.Helper()
	varargs := []any{arg0, arg1, arg2}
	for _, a := range arg3 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "NewStream", varargs...)
	ret0, _ := ret[0].(grpc.ClientStream)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// NewStream indicates an expected call of NewStream.
func (mr *MockPoolConnMockRecorder) NewStream(arg0, arg1, arg2 any, arg3 ...any) *MockPoolConnNewStreamCall {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{arg0, arg1, arg2}, arg3...)
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "NewStream", reflect.TypeOf((*MockPoolConn)(nil).NewStream), varargs...)
	return &MockPoolConnNewStreamCall{Call: call}
}

// MockPoolConnNewStreamCall wrap *gomock.Call
type MockPoolConnNewStreamCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockPoolConnNewStreamCall) Return(arg0 grpc.ClientStream, arg1 error) *MockPoolConnNewStreamCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockPoolConnNewStreamCall) Do(f func(context.Context, *grpc.StreamDesc, string, ...grpc.CallOption) (grpc.ClientStream, error)) *MockPoolConnNewStreamCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockPoolConnNewStreamCall) DoAndReturn(f func(context.Context, *grpc.StreamDesc, string, ...grpc.CallOption) (grpc.ClientStream, error)) *MockPoolConnNewStreamCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockPoolInterface is a mock of PoolInterface interface.
type MockPoolInterface struct {
	ctrl     *gomock.Controller
	recorder *MockPoolInterfaceMockRecorder
}

// MockPoolInterfaceMockRecorder is the mock recorder for MockPoolInterface.
type MockPoolInterfaceMockRecorder struct {
	mock *MockPoolInterface
}

// NewMockPoolInterface creates a new mock instance.
func NewMockPoolInterface(ctrl *gomock.Controller) *MockPoolInterface {
	mock := &MockPoolInterface{ctrl: ctrl}
	mock.recorder = &MockPoolInterfaceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockPoolInterface) EXPECT() *MockPoolInterfaceMockRecorder {
	return m.recorder
}

// Close mocks base method.
func (m *MockPoolInterface) Close() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Close")
	ret0, _ := ret[0].(error)
	return ret0
}

// Close indicates an expected call of Close.
func (mr *MockPoolInterfaceMockRecorder) Close() *MockPoolInterfaceCloseCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Close", reflect.TypeOf((*MockPoolInterface)(nil).Close))
	return &MockPoolInterfaceCloseCall{Call: call}
}

// MockPoolInterfaceCloseCall wrap *gomock.Call
type MockPoolInterfaceCloseCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockPoolInterfaceCloseCall) Return(arg0 error) *MockPoolInterfaceCloseCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockPoolInterfaceCloseCall) Do(f func() error) *MockPoolInterfaceCloseCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockPoolInterfaceCloseCall) DoAndReturn(f func() error) *MockPoolInterfaceCloseCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Dial mocks base method.
func (m *MockPoolInterface) Dial(arg0 context.Context, arg1 string) (grpctool.PoolConn, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Dial", arg0, arg1)
	ret0, _ := ret[0].(grpctool.PoolConn)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Dial indicates an expected call of Dial.
func (mr *MockPoolInterfaceMockRecorder) Dial(arg0, arg1 any) *MockPoolInterfaceDialCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Dial", reflect.TypeOf((*MockPoolInterface)(nil).Dial), arg0, arg1)
	return &MockPoolInterfaceDialCall{Call: call}
}

// MockPoolInterfaceDialCall wrap *gomock.Call
type MockPoolInterfaceDialCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockPoolInterfaceDialCall) Return(arg0 grpctool.PoolConn, arg1 error) *MockPoolInterfaceDialCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockPoolInterfaceDialCall) Do(f func(context.Context, string) (grpctool.PoolConn, error)) *MockPoolInterfaceDialCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockPoolInterfaceDialCall) DoAndReturn(f func(context.Context, string) (grpctool.PoolConn, error)) *MockPoolInterfaceDialCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockServerErrorReporter is a mock of ServerErrorReporter interface.
type MockServerErrorReporter struct {
	ctrl     *gomock.Controller
	recorder *MockServerErrorReporterMockRecorder
}

// MockServerErrorReporterMockRecorder is the mock recorder for MockServerErrorReporter.
type MockServerErrorReporterMockRecorder struct {
	mock *MockServerErrorReporter
}

// NewMockServerErrorReporter creates a new mock instance.
func NewMockServerErrorReporter(ctrl *gomock.Controller) *MockServerErrorReporter {
	mock := &MockServerErrorReporter{ctrl: ctrl}
	mock.recorder = &MockServerErrorReporterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockServerErrorReporter) EXPECT() *MockServerErrorReporterMockRecorder {
	return m.recorder
}

// Report mocks base method.
func (m *MockServerErrorReporter) Report(arg0 context.Context, arg1 string, arg2 error) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Report", arg0, arg1, arg2)
}

// Report indicates an expected call of Report.
func (mr *MockServerErrorReporterMockRecorder) Report(arg0, arg1, arg2 any) *MockServerErrorReporterReportCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Report", reflect.TypeOf((*MockServerErrorReporter)(nil).Report), arg0, arg1, arg2)
	return &MockServerErrorReporterReportCall{Call: call}
}

// MockServerErrorReporterReportCall wrap *gomock.Call
type MockServerErrorReporterReportCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerErrorReporterReportCall) Return() *MockServerErrorReporterReportCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerErrorReporterReportCall) Do(f func(context.Context, string, error)) *MockServerErrorReporterReportCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerErrorReporterReportCall) DoAndReturn(f func(context.Context, string, error)) *MockServerErrorReporterReportCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}
