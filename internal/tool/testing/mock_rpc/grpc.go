// Code generated by MockGen. DO NOT EDIT.
// Source: google.golang.org/grpc (interfaces: ServerStream,ClientStream,ClientConnInterface,ServerTransportStream)
//
// Generated by this command:
//
//	mockgen -typed -destination grpc.go -package mock_rpc google.golang.org/grpc ServerStream,ClientStream,ClientConnInterface,ServerTransportStream
//

// Package mock_rpc is a generated GoMock package.
package mock_rpc

import (
	context "context"
	reflect "reflect"

	gomock "go.uber.org/mock/gomock"
	grpc "google.golang.org/grpc"
	metadata "google.golang.org/grpc/metadata"
)

// MockServerStream is a mock of ServerStream interface.
type MockServerStream struct {
	ctrl     *gomock.Controller
	recorder *MockServerStreamMockRecorder
}

// MockServerStreamMockRecorder is the mock recorder for MockServerStream.
type MockServerStreamMockRecorder struct {
	mock *MockServerStream
}

// NewMockServerStream creates a new mock instance.
func NewMockServerStream(ctrl *gomock.Controller) *MockServerStream {
	mock := &MockServerStream{ctrl: ctrl}
	mock.recorder = &MockServerStreamMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockServerStream) EXPECT() *MockServerStreamMockRecorder {
	return m.recorder
}

// Context mocks base method.
func (m *MockServerStream) Context() context.Context {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Context")
	ret0, _ := ret[0].(context.Context)
	return ret0
}

// Context indicates an expected call of Context.
func (mr *MockServerStreamMockRecorder) Context() *MockServerStreamContextCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Context", reflect.TypeOf((*MockServerStream)(nil).Context))
	return &MockServerStreamContextCall{Call: call}
}

// MockServerStreamContextCall wrap *gomock.Call
type MockServerStreamContextCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerStreamContextCall) Return(arg0 context.Context) *MockServerStreamContextCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerStreamContextCall) Do(f func() context.Context) *MockServerStreamContextCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerStreamContextCall) DoAndReturn(f func() context.Context) *MockServerStreamContextCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// RecvMsg mocks base method.
func (m *MockServerStream) RecvMsg(arg0 any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RecvMsg", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// RecvMsg indicates an expected call of RecvMsg.
func (mr *MockServerStreamMockRecorder) RecvMsg(arg0 any) *MockServerStreamRecvMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RecvMsg", reflect.TypeOf((*MockServerStream)(nil).RecvMsg), arg0)
	return &MockServerStreamRecvMsgCall{Call: call}
}

// MockServerStreamRecvMsgCall wrap *gomock.Call
type MockServerStreamRecvMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerStreamRecvMsgCall) Return(arg0 error) *MockServerStreamRecvMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerStreamRecvMsgCall) Do(f func(any) error) *MockServerStreamRecvMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerStreamRecvMsgCall) DoAndReturn(f func(any) error) *MockServerStreamRecvMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendHeader mocks base method.
func (m *MockServerStream) SendHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SendHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendHeader indicates an expected call of SendHeader.
func (mr *MockServerStreamMockRecorder) SendHeader(arg0 any) *MockServerStreamSendHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendHeader", reflect.TypeOf((*MockServerStream)(nil).SendHeader), arg0)
	return &MockServerStreamSendHeaderCall{Call: call}
}

// MockServerStreamSendHeaderCall wrap *gomock.Call
type MockServerStreamSendHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerStreamSendHeaderCall) Return(arg0 error) *MockServerStreamSendHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerStreamSendHeaderCall) Do(f func(metadata.MD) error) *MockServerStreamSendHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerStreamSendHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockServerStreamSendHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendMsg mocks base method.
func (m *MockServerStream) SendMsg(arg0 any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SendMsg", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendMsg indicates an expected call of SendMsg.
func (mr *MockServerStreamMockRecorder) SendMsg(arg0 any) *MockServerStreamSendMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendMsg", reflect.TypeOf((*MockServerStream)(nil).SendMsg), arg0)
	return &MockServerStreamSendMsgCall{Call: call}
}

// MockServerStreamSendMsgCall wrap *gomock.Call
type MockServerStreamSendMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerStreamSendMsgCall) Return(arg0 error) *MockServerStreamSendMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerStreamSendMsgCall) Do(f func(any) error) *MockServerStreamSendMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerStreamSendMsgCall) DoAndReturn(f func(any) error) *MockServerStreamSendMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetHeader mocks base method.
func (m *MockServerStream) SetHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetHeader indicates an expected call of SetHeader.
func (mr *MockServerStreamMockRecorder) SetHeader(arg0 any) *MockServerStreamSetHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetHeader", reflect.TypeOf((*MockServerStream)(nil).SetHeader), arg0)
	return &MockServerStreamSetHeaderCall{Call: call}
}

// MockServerStreamSetHeaderCall wrap *gomock.Call
type MockServerStreamSetHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerStreamSetHeaderCall) Return(arg0 error) *MockServerStreamSetHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerStreamSetHeaderCall) Do(f func(metadata.MD) error) *MockServerStreamSetHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerStreamSetHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockServerStreamSetHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetTrailer mocks base method.
func (m *MockServerStream) SetTrailer(arg0 metadata.MD) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SetTrailer", arg0)
}

// SetTrailer indicates an expected call of SetTrailer.
func (mr *MockServerStreamMockRecorder) SetTrailer(arg0 any) *MockServerStreamSetTrailerCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetTrailer", reflect.TypeOf((*MockServerStream)(nil).SetTrailer), arg0)
	return &MockServerStreamSetTrailerCall{Call: call}
}

// MockServerStreamSetTrailerCall wrap *gomock.Call
type MockServerStreamSetTrailerCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerStreamSetTrailerCall) Return() *MockServerStreamSetTrailerCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerStreamSetTrailerCall) Do(f func(metadata.MD)) *MockServerStreamSetTrailerCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerStreamSetTrailerCall) DoAndReturn(f func(metadata.MD)) *MockServerStreamSetTrailerCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockClientStream is a mock of ClientStream interface.
type MockClientStream struct {
	ctrl     *gomock.Controller
	recorder *MockClientStreamMockRecorder
}

// MockClientStreamMockRecorder is the mock recorder for MockClientStream.
type MockClientStreamMockRecorder struct {
	mock *MockClientStream
}

// NewMockClientStream creates a new mock instance.
func NewMockClientStream(ctrl *gomock.Controller) *MockClientStream {
	mock := &MockClientStream{ctrl: ctrl}
	mock.recorder = &MockClientStreamMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockClientStream) EXPECT() *MockClientStreamMockRecorder {
	return m.recorder
}

// CloseSend mocks base method.
func (m *MockClientStream) CloseSend() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CloseSend")
	ret0, _ := ret[0].(error)
	return ret0
}

// CloseSend indicates an expected call of CloseSend.
func (mr *MockClientStreamMockRecorder) CloseSend() *MockClientStreamCloseSendCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CloseSend", reflect.TypeOf((*MockClientStream)(nil).CloseSend))
	return &MockClientStreamCloseSendCall{Call: call}
}

// MockClientStreamCloseSendCall wrap *gomock.Call
type MockClientStreamCloseSendCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientStreamCloseSendCall) Return(arg0 error) *MockClientStreamCloseSendCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientStreamCloseSendCall) Do(f func() error) *MockClientStreamCloseSendCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientStreamCloseSendCall) DoAndReturn(f func() error) *MockClientStreamCloseSendCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Context mocks base method.
func (m *MockClientStream) Context() context.Context {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Context")
	ret0, _ := ret[0].(context.Context)
	return ret0
}

// Context indicates an expected call of Context.
func (mr *MockClientStreamMockRecorder) Context() *MockClientStreamContextCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Context", reflect.TypeOf((*MockClientStream)(nil).Context))
	return &MockClientStreamContextCall{Call: call}
}

// MockClientStreamContextCall wrap *gomock.Call
type MockClientStreamContextCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientStreamContextCall) Return(arg0 context.Context) *MockClientStreamContextCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientStreamContextCall) Do(f func() context.Context) *MockClientStreamContextCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientStreamContextCall) DoAndReturn(f func() context.Context) *MockClientStreamContextCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Header mocks base method.
func (m *MockClientStream) Header() (metadata.MD, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Header")
	ret0, _ := ret[0].(metadata.MD)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Header indicates an expected call of Header.
func (mr *MockClientStreamMockRecorder) Header() *MockClientStreamHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Header", reflect.TypeOf((*MockClientStream)(nil).Header))
	return &MockClientStreamHeaderCall{Call: call}
}

// MockClientStreamHeaderCall wrap *gomock.Call
type MockClientStreamHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientStreamHeaderCall) Return(arg0 metadata.MD, arg1 error) *MockClientStreamHeaderCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientStreamHeaderCall) Do(f func() (metadata.MD, error)) *MockClientStreamHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientStreamHeaderCall) DoAndReturn(f func() (metadata.MD, error)) *MockClientStreamHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// RecvMsg mocks base method.
func (m *MockClientStream) RecvMsg(arg0 any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RecvMsg", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// RecvMsg indicates an expected call of RecvMsg.
func (mr *MockClientStreamMockRecorder) RecvMsg(arg0 any) *MockClientStreamRecvMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RecvMsg", reflect.TypeOf((*MockClientStream)(nil).RecvMsg), arg0)
	return &MockClientStreamRecvMsgCall{Call: call}
}

// MockClientStreamRecvMsgCall wrap *gomock.Call
type MockClientStreamRecvMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientStreamRecvMsgCall) Return(arg0 error) *MockClientStreamRecvMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientStreamRecvMsgCall) Do(f func(any) error) *MockClientStreamRecvMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientStreamRecvMsgCall) DoAndReturn(f func(any) error) *MockClientStreamRecvMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendMsg mocks base method.
func (m *MockClientStream) SendMsg(arg0 any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SendMsg", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendMsg indicates an expected call of SendMsg.
func (mr *MockClientStreamMockRecorder) SendMsg(arg0 any) *MockClientStreamSendMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendMsg", reflect.TypeOf((*MockClientStream)(nil).SendMsg), arg0)
	return &MockClientStreamSendMsgCall{Call: call}
}

// MockClientStreamSendMsgCall wrap *gomock.Call
type MockClientStreamSendMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientStreamSendMsgCall) Return(arg0 error) *MockClientStreamSendMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientStreamSendMsgCall) Do(f func(any) error) *MockClientStreamSendMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientStreamSendMsgCall) DoAndReturn(f func(any) error) *MockClientStreamSendMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Trailer mocks base method.
func (m *MockClientStream) Trailer() metadata.MD {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Trailer")
	ret0, _ := ret[0].(metadata.MD)
	return ret0
}

// Trailer indicates an expected call of Trailer.
func (mr *MockClientStreamMockRecorder) Trailer() *MockClientStreamTrailerCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Trailer", reflect.TypeOf((*MockClientStream)(nil).Trailer))
	return &MockClientStreamTrailerCall{Call: call}
}

// MockClientStreamTrailerCall wrap *gomock.Call
type MockClientStreamTrailerCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientStreamTrailerCall) Return(arg0 metadata.MD) *MockClientStreamTrailerCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientStreamTrailerCall) Do(f func() metadata.MD) *MockClientStreamTrailerCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientStreamTrailerCall) DoAndReturn(f func() metadata.MD) *MockClientStreamTrailerCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockClientConnInterface is a mock of ClientConnInterface interface.
type MockClientConnInterface struct {
	ctrl     *gomock.Controller
	recorder *MockClientConnInterfaceMockRecorder
}

// MockClientConnInterfaceMockRecorder is the mock recorder for MockClientConnInterface.
type MockClientConnInterfaceMockRecorder struct {
	mock *MockClientConnInterface
}

// NewMockClientConnInterface creates a new mock instance.
func NewMockClientConnInterface(ctrl *gomock.Controller) *MockClientConnInterface {
	mock := &MockClientConnInterface{ctrl: ctrl}
	mock.recorder = &MockClientConnInterfaceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockClientConnInterface) EXPECT() *MockClientConnInterfaceMockRecorder {
	return m.recorder
}

// Invoke mocks base method.
func (m *MockClientConnInterface) Invoke(arg0 context.Context, arg1 string, arg2, arg3 any, arg4 ...grpc.CallOption) error {
	m.ctrl.T.Helper()
	varargs := []any{arg0, arg1, arg2, arg3}
	for _, a := range arg4 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "Invoke", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Invoke indicates an expected call of Invoke.
func (mr *MockClientConnInterfaceMockRecorder) Invoke(arg0, arg1, arg2, arg3 any, arg4 ...any) *MockClientConnInterfaceInvokeCall {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{arg0, arg1, arg2, arg3}, arg4...)
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Invoke", reflect.TypeOf((*MockClientConnInterface)(nil).Invoke), varargs...)
	return &MockClientConnInterfaceInvokeCall{Call: call}
}

// MockClientConnInterfaceInvokeCall wrap *gomock.Call
type MockClientConnInterfaceInvokeCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientConnInterfaceInvokeCall) Return(arg0 error) *MockClientConnInterfaceInvokeCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientConnInterfaceInvokeCall) Do(f func(context.Context, string, any, any, ...grpc.CallOption) error) *MockClientConnInterfaceInvokeCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientConnInterfaceInvokeCall) DoAndReturn(f func(context.Context, string, any, any, ...grpc.CallOption) error) *MockClientConnInterfaceInvokeCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// NewStream mocks base method.
func (m *MockClientConnInterface) NewStream(arg0 context.Context, arg1 *grpc.StreamDesc, arg2 string, arg3 ...grpc.CallOption) (grpc.ClientStream, error) {
	m.ctrl.T.Helper()
	varargs := []any{arg0, arg1, arg2}
	for _, a := range arg3 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "NewStream", varargs...)
	ret0, _ := ret[0].(grpc.ClientStream)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// NewStream indicates an expected call of NewStream.
func (mr *MockClientConnInterfaceMockRecorder) NewStream(arg0, arg1, arg2 any, arg3 ...any) *MockClientConnInterfaceNewStreamCall {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{arg0, arg1, arg2}, arg3...)
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "NewStream", reflect.TypeOf((*MockClientConnInterface)(nil).NewStream), varargs...)
	return &MockClientConnInterfaceNewStreamCall{Call: call}
}

// MockClientConnInterfaceNewStreamCall wrap *gomock.Call
type MockClientConnInterfaceNewStreamCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockClientConnInterfaceNewStreamCall) Return(arg0 grpc.ClientStream, arg1 error) *MockClientConnInterfaceNewStreamCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockClientConnInterfaceNewStreamCall) Do(f func(context.Context, *grpc.StreamDesc, string, ...grpc.CallOption) (grpc.ClientStream, error)) *MockClientConnInterfaceNewStreamCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockClientConnInterfaceNewStreamCall) DoAndReturn(f func(context.Context, *grpc.StreamDesc, string, ...grpc.CallOption) (grpc.ClientStream, error)) *MockClientConnInterfaceNewStreamCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockServerTransportStream is a mock of ServerTransportStream interface.
type MockServerTransportStream struct {
	ctrl     *gomock.Controller
	recorder *MockServerTransportStreamMockRecorder
}

// MockServerTransportStreamMockRecorder is the mock recorder for MockServerTransportStream.
type MockServerTransportStreamMockRecorder struct {
	mock *MockServerTransportStream
}

// NewMockServerTransportStream creates a new mock instance.
func NewMockServerTransportStream(ctrl *gomock.Controller) *MockServerTransportStream {
	mock := &MockServerTransportStream{ctrl: ctrl}
	mock.recorder = &MockServerTransportStreamMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockServerTransportStream) EXPECT() *MockServerTransportStreamMockRecorder {
	return m.recorder
}

// Method mocks base method.
func (m *MockServerTransportStream) Method() string {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Method")
	ret0, _ := ret[0].(string)
	return ret0
}

// Method indicates an expected call of Method.
func (mr *MockServerTransportStreamMockRecorder) Method() *MockServerTransportStreamMethodCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Method", reflect.TypeOf((*MockServerTransportStream)(nil).Method))
	return &MockServerTransportStreamMethodCall{Call: call}
}

// MockServerTransportStreamMethodCall wrap *gomock.Call
type MockServerTransportStreamMethodCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerTransportStreamMethodCall) Return(arg0 string) *MockServerTransportStreamMethodCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerTransportStreamMethodCall) Do(f func() string) *MockServerTransportStreamMethodCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerTransportStreamMethodCall) DoAndReturn(f func() string) *MockServerTransportStreamMethodCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendHeader mocks base method.
func (m *MockServerTransportStream) SendHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SendHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendHeader indicates an expected call of SendHeader.
func (mr *MockServerTransportStreamMockRecorder) SendHeader(arg0 any) *MockServerTransportStreamSendHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendHeader", reflect.TypeOf((*MockServerTransportStream)(nil).SendHeader), arg0)
	return &MockServerTransportStreamSendHeaderCall{Call: call}
}

// MockServerTransportStreamSendHeaderCall wrap *gomock.Call
type MockServerTransportStreamSendHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerTransportStreamSendHeaderCall) Return(arg0 error) *MockServerTransportStreamSendHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerTransportStreamSendHeaderCall) Do(f func(metadata.MD) error) *MockServerTransportStreamSendHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerTransportStreamSendHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockServerTransportStreamSendHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetHeader mocks base method.
func (m *MockServerTransportStream) SetHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetHeader indicates an expected call of SetHeader.
func (mr *MockServerTransportStreamMockRecorder) SetHeader(arg0 any) *MockServerTransportStreamSetHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetHeader", reflect.TypeOf((*MockServerTransportStream)(nil).SetHeader), arg0)
	return &MockServerTransportStreamSetHeaderCall{Call: call}
}

// MockServerTransportStreamSetHeaderCall wrap *gomock.Call
type MockServerTransportStreamSetHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerTransportStreamSetHeaderCall) Return(arg0 error) *MockServerTransportStreamSetHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerTransportStreamSetHeaderCall) Do(f func(metadata.MD) error) *MockServerTransportStreamSetHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerTransportStreamSetHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockServerTransportStreamSetHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetTrailer mocks base method.
func (m *MockServerTransportStream) SetTrailer(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetTrailer", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetTrailer indicates an expected call of SetTrailer.
func (mr *MockServerTransportStreamMockRecorder) SetTrailer(arg0 any) *MockServerTransportStreamSetTrailerCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetTrailer", reflect.TypeOf((*MockServerTransportStream)(nil).SetTrailer), arg0)
	return &MockServerTransportStreamSetTrailerCall{Call: call}
}

// MockServerTransportStreamSetTrailerCall wrap *gomock.Call
type MockServerTransportStreamSetTrailerCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockServerTransportStreamSetTrailerCall) Return(arg0 error) *MockServerTransportStreamSetTrailerCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockServerTransportStreamSetTrailerCall) Do(f func(metadata.MD) error) *MockServerTransportStreamSetTrailerCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockServerTransportStreamSetTrailerCall) DoAndReturn(f func(metadata.MD) error) *MockServerTransportStreamSetTrailerCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}
