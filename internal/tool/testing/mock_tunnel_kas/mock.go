// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/kas (interfaces: Handler,Tracker,Querier)
//
// Generated by this command:
//
//	mockgen -typed -destination mock.go -package mock_tunnel_kas gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/kas Handler,Tracker,Querier
//

// Package mock_tunnel_kas is a generated GoMock package.
package mock_tunnel_kas

import (
	context "context"
	reflect "reflect"
	time "time"

	api "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/api"
	rpc "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/rpc"
	gomock "go.uber.org/mock/gomock"
)

// MockHandler is a mock of Handler interface.
type MockHandler struct {
	ctrl     *gomock.Controller
	recorder *MockHandlerMockRecorder
}

// MockHandlerMockRecorder is the mock recorder for MockHandler.
type MockHandlerMockRecorder struct {
	mock *MockHandler
}

// NewMockHandler creates a new mock instance.
func NewMockHandler(ctrl *gomock.Controller) *MockHandler {
	mock := &MockHandler{ctrl: ctrl}
	mock.recorder = &MockHandlerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockHandler) EXPECT() *MockHandlerMockRecorder {
	return m.recorder
}

// HandleTunnel mocks base method.
func (m *MockHandler) HandleTunnel(arg0 context.Context, arg1 *api.AgentInfo, arg2 rpc.ReverseTunnel_ConnectServer) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HandleTunnel", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// HandleTunnel indicates an expected call of HandleTunnel.
func (mr *MockHandlerMockRecorder) HandleTunnel(arg0, arg1, arg2 any) *MockHandlerHandleTunnelCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HandleTunnel", reflect.TypeOf((*MockHandler)(nil).HandleTunnel), arg0, arg1, arg2)
	return &MockHandlerHandleTunnelCall{Call: call}
}

// MockHandlerHandleTunnelCall wrap *gomock.Call
type MockHandlerHandleTunnelCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockHandlerHandleTunnelCall) Return(arg0 error) *MockHandlerHandleTunnelCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockHandlerHandleTunnelCall) Do(f func(context.Context, *api.AgentInfo, rpc.ReverseTunnel_ConnectServer) error) *MockHandlerHandleTunnelCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockHandlerHandleTunnelCall) DoAndReturn(f func(context.Context, *api.AgentInfo, rpc.ReverseTunnel_ConnectServer) error) *MockHandlerHandleTunnelCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockTracker is a mock of Tracker interface.
type MockTracker struct {
	ctrl     *gomock.Controller
	recorder *MockTrackerMockRecorder
}

// MockTrackerMockRecorder is the mock recorder for MockTracker.
type MockTrackerMockRecorder struct {
	mock *MockTracker
}

// NewMockTracker creates a new mock instance.
func NewMockTracker(ctrl *gomock.Controller) *MockTracker {
	mock := &MockTracker{ctrl: ctrl}
	mock.recorder = &MockTrackerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockTracker) EXPECT() *MockTrackerMockRecorder {
	return m.recorder
}

// KASURLsByAgentID mocks base method.
func (m *MockTracker) KASURLsByAgentID(arg0 context.Context, arg1 int64) ([]string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "KASURLsByAgentID", arg0, arg1)
	ret0, _ := ret[0].([]string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// KASURLsByAgentID indicates an expected call of KASURLsByAgentID.
func (mr *MockTrackerMockRecorder) KASURLsByAgentID(arg0, arg1 any) *MockTrackerKASURLsByAgentIDCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "KASURLsByAgentID", reflect.TypeOf((*MockTracker)(nil).KASURLsByAgentID), arg0, arg1)
	return &MockTrackerKASURLsByAgentIDCall{Call: call}
}

// MockTrackerKASURLsByAgentIDCall wrap *gomock.Call
type MockTrackerKASURLsByAgentIDCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockTrackerKASURLsByAgentIDCall) Return(arg0 []string, arg1 error) *MockTrackerKASURLsByAgentIDCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockTrackerKASURLsByAgentIDCall) Do(f func(context.Context, int64) ([]string, error)) *MockTrackerKASURLsByAgentIDCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockTrackerKASURLsByAgentIDCall) DoAndReturn(f func(context.Context, int64) ([]string, error)) *MockTrackerKASURLsByAgentIDCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Refresh mocks base method.
func (m *MockTracker) Refresh(arg0 context.Context, arg1 time.Duration, arg2 ...int64) error {
	m.ctrl.T.Helper()
	varargs := []any{arg0, arg1}
	for _, a := range arg2 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "Refresh", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Refresh indicates an expected call of Refresh.
func (mr *MockTrackerMockRecorder) Refresh(arg0, arg1 any, arg2 ...any) *MockTrackerRefreshCall {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{arg0, arg1}, arg2...)
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Refresh", reflect.TypeOf((*MockTracker)(nil).Refresh), varargs...)
	return &MockTrackerRefreshCall{Call: call}
}

// MockTrackerRefreshCall wrap *gomock.Call
type MockTrackerRefreshCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockTrackerRefreshCall) Return(arg0 error) *MockTrackerRefreshCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockTrackerRefreshCall) Do(f func(context.Context, time.Duration, ...int64) error) *MockTrackerRefreshCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockTrackerRefreshCall) DoAndReturn(f func(context.Context, time.Duration, ...int64) error) *MockTrackerRefreshCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// RegisterTunnel mocks base method.
func (m *MockTracker) RegisterTunnel(arg0 context.Context, arg1 time.Duration, arg2 int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RegisterTunnel", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// RegisterTunnel indicates an expected call of RegisterTunnel.
func (mr *MockTrackerMockRecorder) RegisterTunnel(arg0, arg1, arg2 any) *MockTrackerRegisterTunnelCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RegisterTunnel", reflect.TypeOf((*MockTracker)(nil).RegisterTunnel), arg0, arg1, arg2)
	return &MockTrackerRegisterTunnelCall{Call: call}
}

// MockTrackerRegisterTunnelCall wrap *gomock.Call
type MockTrackerRegisterTunnelCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockTrackerRegisterTunnelCall) Return(arg0 error) *MockTrackerRegisterTunnelCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockTrackerRegisterTunnelCall) Do(f func(context.Context, time.Duration, int64) error) *MockTrackerRegisterTunnelCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockTrackerRegisterTunnelCall) DoAndReturn(f func(context.Context, time.Duration, int64) error) *MockTrackerRegisterTunnelCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// UnregisterTunnel mocks base method.
func (m *MockTracker) UnregisterTunnel(arg0 context.Context, arg1 int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UnregisterTunnel", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// UnregisterTunnel indicates an expected call of UnregisterTunnel.
func (mr *MockTrackerMockRecorder) UnregisterTunnel(arg0, arg1 any) *MockTrackerUnregisterTunnelCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UnregisterTunnel", reflect.TypeOf((*MockTracker)(nil).UnregisterTunnel), arg0, arg1)
	return &MockTrackerUnregisterTunnelCall{Call: call}
}

// MockTrackerUnregisterTunnelCall wrap *gomock.Call
type MockTrackerUnregisterTunnelCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockTrackerUnregisterTunnelCall) Return(arg0 error) *MockTrackerUnregisterTunnelCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockTrackerUnregisterTunnelCall) Do(f func(context.Context, int64) error) *MockTrackerUnregisterTunnelCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockTrackerUnregisterTunnelCall) DoAndReturn(f func(context.Context, int64) error) *MockTrackerUnregisterTunnelCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockQuerier is a mock of Querier interface.
type MockQuerier struct {
	ctrl     *gomock.Controller
	recorder *MockQuerierMockRecorder
}

// MockQuerierMockRecorder is the mock recorder for MockQuerier.
type MockQuerierMockRecorder struct {
	mock *MockQuerier
}

// NewMockQuerier creates a new mock instance.
func NewMockQuerier(ctrl *gomock.Controller) *MockQuerier {
	mock := &MockQuerier{ctrl: ctrl}
	mock.recorder = &MockQuerierMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockQuerier) EXPECT() *MockQuerierMockRecorder {
	return m.recorder
}

// KASURLsByAgentID mocks base method.
func (m *MockQuerier) KASURLsByAgentID(arg0 context.Context, arg1 int64) ([]string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "KASURLsByAgentID", arg0, arg1)
	ret0, _ := ret[0].([]string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// KASURLsByAgentID indicates an expected call of KASURLsByAgentID.
func (mr *MockQuerierMockRecorder) KASURLsByAgentID(arg0, arg1 any) *MockQuerierKASURLsByAgentIDCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "KASURLsByAgentID", reflect.TypeOf((*MockQuerier)(nil).KASURLsByAgentID), arg0, arg1)
	return &MockQuerierKASURLsByAgentIDCall{Call: call}
}

// MockQuerierKASURLsByAgentIDCall wrap *gomock.Call
type MockQuerierKASURLsByAgentIDCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockQuerierKASURLsByAgentIDCall) Return(arg0 []string, arg1 error) *MockQuerierKASURLsByAgentIDCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockQuerierKASURLsByAgentIDCall) Do(f func(context.Context, int64) ([]string, error)) *MockQuerierKASURLsByAgentIDCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockQuerierKASURLsByAgentIDCall) DoAndReturn(f func(context.Context, int64) ([]string, error)) *MockQuerierKASURLsByAgentIDCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}
