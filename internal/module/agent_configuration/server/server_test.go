package server

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/gitaly"
	gapi "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/gitlab/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_configuration"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_configuration/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/matcher"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_gitlab"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_internalgitaly"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/pkg/agentcfg"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/pkg/event"
	"go.uber.org/mock/gomock"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/testing/protocmp"
	"sigs.k8s.io/yaml"
)

const (
	revision     = "507ebc6de9bcac25628aa7afd52802a91a0685d8"
	branchPrefix = "refs/heads/"

	maxConfigurationFileSize = 128 * 1024
)

var (
	projectID = "some/project"

	_ modserver.Factory            = (*Factory)(nil)
	_ modserver.ApplyDefaults      = ApplyDefaults
	_ rpc.AgentConfigurationServer = (*server)(nil)
)

func TestEmptyConfig(t *testing.T) {
	t.Run("comments", func(t *testing.T) {
		data := []byte(`
#gitops:
#  manifest_projects:
#  - id: "root/gitops-manifests"
#    paths:
#      - glob: "/bla/**"
`)
		assertEmpty(t, data)
	})
	t.Run("empty", func(t *testing.T) {
		data := []byte("")
		assertEmpty(t, data)
	})
	t.Run("newline", func(t *testing.T) {
		data := []byte("\n")
		assertEmpty(t, data)
	})
	t.Run("missing", func(t *testing.T) {
		var data []byte
		assertEmpty(t, data)
	})
}

func assertEmpty(t *testing.T, data []byte) {
	config, err := parseYAMLToConfiguration(data)
	require.NoError(t, err)
	diff := cmp.Diff(config, &agentcfg.ConfigurationFile{}, protocmp.Transform())
	assert.Empty(t, diff)
}

func TestYAMLToConfigurationAndBack(t *testing.T) {
	testCases := []struct {
		given, expected string
	}{
		{
			given: `{}
`, // empty config
			expected: `{}
`,
		},
		{
			given: `gitops: {}
`,
			expected: `gitops: {}
`,
		},
		{
			given: `gitops:
  manifest_projects: []
`,
			expected: `gitops: {}
`, // empty slice is omitted
		},
		{
			expected: `gitops:
  manifest_projects:
  - id: gitlab-org/cluster-integration/gitlab-agent
`,
			given: `gitops:
  manifest_projects:
  - id: gitlab-org/cluster-integration/gitlab-agent
`,
		},
	}

	for i, tc := range testCases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			config, err := parseYAMLToConfiguration([]byte(tc.given))
			require.NoError(t, err)
			configJSON, err := protojson.Marshal(config)
			require.NoError(t, err)
			configYAML, err := yaml.JSONToYAML(configJSON)
			require.NoError(t, err)
			diff := cmp.Diff(tc.expected, string(configYAML))
			assert.Empty(t, diff)
		})
	}
}

func TestGetConfiguration_HappyPath(t *testing.T) {
	s, agentInfo, ctrl, gitalyPool, resp, _ := setupServer(t)
	configFile := sampleConfig()
	resp.EXPECT().
		Send(matcher.ProtoEq(t, &rpc.ConfigurationResponse{
			Configuration: &agentcfg.AgentConfiguration{
				Gitops: &agentcfg.GitopsCF{
					ManifestProjects: []*agentcfg.ManifestProjectCF{
						{
							Id: &projectID,
						},
					},
				},
				AgentId:     agentInfo.ID,
				ProjectId:   agentInfo.ProjectID,
				ProjectPath: agentInfo.Repository.GlProjectPath,
			},
			CommitId: revision,
		}))
	p := mock_internalgitaly.NewMockPollerInterface(ctrl)
	pf := mock_internalgitaly.NewMockPathFetcherInterface(ctrl)
	configFileName := agent_configuration.Directory + "/" + agentInfo.Name + "/" + agent_configuration.FileName
	gomock.InOrder(
		gitalyPool.EXPECT().
			Poller(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
			Return(p, nil),
		p.EXPECT().
			Poll(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), "", branchPrefix+agentInfo.DefaultBranch).
			Return(&gitaly.PollInfo{
				CommitID:        revision,
				UpdateAvailable: true,
			}, nil),
		gitalyPool.EXPECT().
			PathFetcher(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
			Return(pf, nil),
		pf.EXPECT().
			FetchFile(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), []byte(revision), []byte(configFileName), int64(maxConfigurationFileSize)).
			Return(configToBytes(t, configFile), nil),
	)
	err := s.GetConfiguration(&rpc.ConfigurationRequest{
		AgentMeta: mock_modserver.AgentMeta(),
	}, resp)
	require.NoError(t, err)
}

func TestGetConfiguration_ResumeConnection(t *testing.T) {
	s, agentInfo, ctrl, gitalyPool, resp, _ := setupServer(t)
	p := mock_internalgitaly.NewMockPollerInterface(ctrl)
	gomock.InOrder(
		gitalyPool.EXPECT().
			Poller(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
			Return(p, nil),
		p.EXPECT().
			Poll(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), revision, branchPrefix+agentInfo.DefaultBranch).
			Return(&gitaly.PollInfo{
				CommitID:        revision,
				UpdateAvailable: false,
			}, nil),
	)
	err := s.GetConfiguration(&rpc.ConfigurationRequest{
		CommitId:  revision, // same commit id
		AgentMeta: mock_modserver.AgentMeta(),
	}, resp)
	require.NoError(t, err)
}

func TestGetConfiguration_RefNotFound(t *testing.T) {
	s, agentInfo, ctrl, gitalyPool, resp, _ := setupServer(t)
	p := mock_internalgitaly.NewMockPollerInterface(ctrl)
	gomock.InOrder(
		gitalyPool.EXPECT().
			Poller(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
			Return(p, nil),
		p.EXPECT().
			Poll(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), "", branchPrefix+agentInfo.DefaultBranch).
			Return(nil, gitaly.NewNotFoundError("Bla", "some/ref")),
	)
	err := s.GetConfiguration(&rpc.ConfigurationRequest{
		AgentMeta: mock_modserver.AgentMeta(),
	}, resp)
	require.EqualError(t, err, "rpc error: code = NotFound desc = config: repository poll failed: NotFound: Bla: file/directory/ref not found: some/ref")
}

func TestGetConfiguration_ConfigNotFound(t *testing.T) {
	s, agentInfo, ctrl, gitalyPool, resp, _ := setupServer(t)
	resp.EXPECT().
		Send(matcher.ProtoEq(t, &rpc.ConfigurationResponse{
			Configuration: &agentcfg.AgentConfiguration{
				AgentId:     agentInfo.ID,
				ProjectId:   agentInfo.ProjectID,
				ProjectPath: agentInfo.Repository.GlProjectPath,
			},
			CommitId: revision,
		}))
	p := mock_internalgitaly.NewMockPollerInterface(ctrl)
	pf := mock_internalgitaly.NewMockPathFetcherInterface(ctrl)
	configFileName := agent_configuration.Directory + "/" + agentInfo.Name + "/" + agent_configuration.FileName
	gomock.InOrder(
		gitalyPool.EXPECT().
			Poller(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
			Return(p, nil),
		p.EXPECT().
			Poll(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), "", branchPrefix+agentInfo.DefaultBranch).
			Return(&gitaly.PollInfo{
				CommitID:        revision,
				UpdateAvailable: true,
			}, nil),
		gitalyPool.EXPECT().
			PathFetcher(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
			Return(pf, nil),
		pf.EXPECT().
			FetchFile(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), []byte(revision), []byte(configFileName), int64(maxConfigurationFileSize)).
			Return(nil, gitaly.NewNotFoundError("Bla", "some/file")),
	)
	err := s.GetConfiguration(&rpc.ConfigurationRequest{
		AgentMeta: mock_modserver.AgentMeta(),
	}, resp)
	require.NoError(t, err)
}

func TestGetConfiguration_EmptyRepository(t *testing.T) {
	s, agentInfo, ctrl, gitalyPool, resp, _ := setupServer(t)
	p := mock_internalgitaly.NewMockPollerInterface(ctrl)
	gomock.InOrder(
		gitalyPool.EXPECT().
			Poller(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
			Return(p, nil),
		p.EXPECT().
			Poll(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), "", branchPrefix+agentInfo.DefaultBranch).
			Return(&gitaly.PollInfo{
				RefNotFound: true,
			}, nil),
	)
	err := s.GetConfiguration(&rpc.ConfigurationRequest{
		AgentMeta: mock_modserver.AgentMeta(),
	}, resp)
	require.NoError(t, err)
}

func TestGetConfiguration_UserErrors(t *testing.T) {
	gitalyErrs := []error{
		gitaly.NewFileTooBigError(nil, "Bla", "some/file"),
		gitaly.NewUnexpectedTreeEntryTypeError("Bla", "some/file"),
	}
	for _, gitalyErr := range gitalyErrs {
		t.Run(gitalyErr.(*gitaly.Error).Code.String(), func(t *testing.T) { //nolint: errorlint
			s, agentInfo, ctrl, gitalyPool, resp, mockRPCAPI := setupServer(t)
			p := mock_internalgitaly.NewMockPollerInterface(ctrl)
			pf := mock_internalgitaly.NewMockPathFetcherInterface(ctrl)
			configFileName := agent_configuration.Directory + "/" + agentInfo.Name + "/" + agent_configuration.FileName
			gomock.InOrder(
				gitalyPool.EXPECT().
					Poller(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
					Return(p, nil),
				p.EXPECT().
					Poll(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), "", branchPrefix+agentInfo.DefaultBranch).
					Return(&gitaly.PollInfo{
						CommitID:        revision,
						UpdateAvailable: true,
					}, nil),
				gitalyPool.EXPECT().
					PathFetcher(gomock.Any(), matcher.ProtoEq(nil, agentInfo.GitalyInfo)).
					Return(pf, nil),
				pf.EXPECT().
					FetchFile(gomock.Any(), matcher.ProtoEq(nil, agentInfo.Repository), []byte(revision), []byte(configFileName), int64(maxConfigurationFileSize)).
					Return(nil, gitalyErr),
				mockRPCAPI.EXPECT().
					HandleProcessingError(gomock.Any(), testhelpers.AgentID, "Config: failed to fetch",
						matcher.ErrorEq(fmt.Sprintf("agent configuration file: %v", gitalyErr))),
			)
			err := s.GetConfiguration(&rpc.ConfigurationRequest{
				AgentMeta: mock_modserver.AgentMeta(),
			}, resp)
			assert.EqualError(t, err, fmt.Sprintf("rpc error: code = FailedPrecondition desc = config: agent configuration file: %v", gitalyErr))
		})
	}
}

func TestGetConfiguration_GetAgentInfo_Error(t *testing.T) {
	s, _, _, resp, mockRPCAPI, _, _ := setupServerBare(t, 1)
	mockRPCAPI.EXPECT().
		AgentInfo(gomock.Any(), gomock.Any()).
		Return(nil, status.Error(codes.PermissionDenied, "expected err")) // code doesn't matter, we test that we return on error
	err := s.GetConfiguration(&rpc.ConfigurationRequest{
		AgentMeta: mock_modserver.AgentMeta(),
	}, resp)
	assert.EqualError(t, err, "rpc error: code = PermissionDenied desc = expected err")
}

func TestGetConfiguration_GetAgentInfo_RetriableError(t *testing.T) {
	s, _, _, resp, mockRPCAPI, _, _ := setupServerBare(t, 2)
	gomock.InOrder(
		mockRPCAPI.EXPECT().
			AgentInfo(gomock.Any(), gomock.Any()).
			Return(nil, status.Error(codes.Unavailable, "unavailable")),
		mockRPCAPI.EXPECT().
			AgentInfo(gomock.Any(), gomock.Any()).
			Return(nil, status.Error(codes.PermissionDenied, "expected err")), // code doesn't matter, we test that we return on error
	)
	err := s.GetConfiguration(&rpc.ConfigurationRequest{
		AgentMeta: mock_modserver.AgentMeta(),
	}, resp)
	assert.EqualError(t, err, "rpc error: code = PermissionDenied desc = expected err")
}

func setupServerBare(t *testing.T, pollTimes int) (*server, *gomock.Controller, *mock_internalgitaly.MockPoolInterface, *mock_rpc.MockAgentConfiguration_GetConfigurationServer, *mock_modserver.MockAgentRPCAPI, *mock_modserver.MockAPI, *mock_agent_tracker.MockTracker) {
	ctrl := gomock.NewController(t)
	mockRPCAPI := mock_modserver.NewMockAgentRPCAPIWithMockPoller(ctrl, pollTimes)
	mockAPI := mock_modserver.NewMockAPI(ctrl)
	gitalyPool := mock_internalgitaly.NewMockPoolInterface(ctrl)
	agentTracker := mock_agent_tracker.NewMockTracker(ctrl)
	gitLabClient := mock_gitlab.SetupClient(t, gapi.AgentConfigurationAPIPath, func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
	})
	s := &server{
		serverAPI:                  mockAPI,
		agentRegisterer:            agentTracker,
		gitaly:                     gitalyPool,
		gitLabClient:               gitLabClient,
		getConfigurationPollConfig: testhelpers.NewPollConfig(10 * time.Minute),
		maxConfigurationFileSize:   maxConfigurationFileSize,
	}
	resp := mock_rpc.NewMockAgentConfiguration_GetConfigurationServer(ctrl)
	resp.EXPECT().
		Context().
		Return(mock_modserver.IncomingAgentCtx(t, mockRPCAPI)).
		MinTimes(1)
	return s, ctrl, gitalyPool, resp, mockRPCAPI, mockAPI, agentTracker
}

func setupServer(t *testing.T) (*server, *api.AgentInfo, *gomock.Controller, *mock_internalgitaly.MockPoolInterface, *mock_rpc.MockAgentConfiguration_GetConfigurationServer, *mock_modserver.MockAgentRPCAPI) {
	s, ctrl, gitalyPool, resp, mockRPCAPI, mockAPI, agentTracker := setupServerBare(t, 1)
	agentInfo := testhelpers.AgentInfoObj()
	connMatcher := matcher.ProtoEq(t, &agent_tracker.ConnectedAgentInfo{
		AgentMeta: mock_modserver.AgentMeta(),
		AgentId:   agentInfo.ID,
		ProjectId: agentInfo.ProjectID,
	}, protocmp.IgnoreFields(&agent_tracker.ConnectedAgentInfo{}, "connected_at", "connection_id"))
	gomock.InOrder(
		mockRPCAPI.EXPECT().
			AgentInfo(gomock.Any(), gomock.Any()).
			Return(agentInfo, nil),
		agentTracker.EXPECT().
			RegisterConnection(gomock.Any(), connMatcher),
	)
	mockAPI.EXPECT().
		OnGitPushEvent(gomock.Any(), gomock.Any()).
		Do(func(ctx context.Context, cb syncz.EventCallback[*event.GitPushEvent]) {
			<-ctx.Done()
		})
	agentTracker.EXPECT().
		UnregisterConnection(gomock.Any(), connMatcher)
	return s, agentInfo, ctrl, gitalyPool, resp, mockRPCAPI
}

func configToBytes(t *testing.T, configFile *agentcfg.ConfigurationFile) []byte {
	configJSON, err := protojson.Marshal(configFile)
	require.NoError(t, err)
	configYAML, err := yaml.JSONToYAML(configJSON)
	require.NoError(t, err)
	return configYAML
}

func sampleConfig() *agentcfg.ConfigurationFile {
	return &agentcfg.ConfigurationFile{
		Gitops: &agentcfg.GitopsCF{
			ManifestProjects: []*agentcfg.ManifestProjectCF{
				{
					Id: &projectID,
				},
			},
		},
	}
}
