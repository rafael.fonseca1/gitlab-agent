package agent

import (
	"context"
	"fmt"
	"net/http"
	"time"

	notificationv1 "github.com/fluxcd/notification-controller/api/v1"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/flux"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/flux/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/retry"
	"go.uber.org/zap"
	apiextensionsv1api "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	apiextensionsv1client "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset/typed/apiextensions/v1"
	kubeerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/transport"
)

const (
	// resyncDuration defines the duration for the shared informer cache resync interval.
	resyncDuration = 10 * time.Minute

	reconcileProjectsInitBackoff   = 10 * time.Second
	reconcileProjectsMaxBackoff    = 5 * time.Minute
	reconcileProjectsResetDuration = 10 * time.Minute
	reconcileProjectsBackoffFactor = 2.0
	reconcileProjectsJitter        = 1.0
)

var (
	requiredFluxCRDs = [...]schema.GroupVersionResource{
		sourcev1.GroupVersion.WithResource("gitrepositories"),
		notificationv1.GroupVersion.WithResource("receivers"),
	}
)

type Factory struct{}

func (f *Factory) IsProducingLeaderModules() bool {
	return true
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	restConfig, err := config.K8sUtilFactory.ToRESTConfig()
	if err != nil {
		return nil, err
	}

	extAPIClient, err := apiextensionsv1client.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}
	if !isFluxInstalled(context.Background(), config.Log, extAPIClient) {
		config.Log.Info("Flux is not installed, skipping module. A restart is required for this to be checked again")
		return nil, nil
	}

	dynamicClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	clientset, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	receiverClient := dynamicClient.Resource(notificationv1.GroupVersion.WithResource("receivers"))

	kubeAPIURL, _, err := defaultServerURLFor(restConfig)
	if err != nil {
		return nil, err
	}
	transportCfg, err := restConfig.TransportConfig()
	if err != nil {
		return nil, err
	}
	kubeAPIRoundTripper, err := transport.New(transportCfg)
	if err != nil {
		return nil, err
	}

	return &module{
		log:             config.Log,
		k8sExtAPIClient: extAPIClient,
		runner: &fluxReconciliationRunner{
			informersFactory: func() (informers.GenericInformer, informers.GenericInformer, cache.Indexer) {
				informerFactory := dynamicinformer.NewDynamicSharedInformerFactory(dynamicClient, resyncDuration)
				gitRepositoryInformer := informerFactory.ForResource(sourcev1.GroupVersion.WithResource("gitrepositories"))
				receiverInformer := informerFactory.ForResource(notificationv1.GroupVersion.WithResource("receivers"))
				receiverIndexer := receiverInformer.Informer().GetIndexer()
				return gitRepositoryInformer, receiverInformer, receiverIndexer
			},
			clientFactory: func(ctx context.Context, cfgURL string, receiverIndexer cache.Indexer) (*client, error) {
				agentID, err := config.API.GetAgentID(ctx)
				if err != nil {
					return nil, err
				}

				rt, err := newGitRepositoryReconcileTrigger(cfgURL, kubeAPIURL, kubeAPIRoundTripper, http.DefaultTransport)
				if err != nil {
					return nil, err
				}

				return newClient(
					config.Log,
					config.API,
					agentID,
					rpc.NewGitLabFluxClient(config.KASConn),
					retry.NewPollConfigFactory(0, retry.NewExponentialBackoffFactory(
						reconcileProjectsInitBackoff, reconcileProjectsMaxBackoff, reconcileProjectsResetDuration, reconcileProjectsBackoffFactor, reconcileProjectsJitter),
					),
					receiverIndexer,
					rt,
				)
			},
			controllerFactory: func(ctx context.Context, gitRepositoryInformer informers.GenericInformer, receiverInformer informers.GenericInformer, projectReconciler projectReconciler) (controller, error) {
				agentID, err := config.API.GetAgentID(ctx)
				if err != nil {
					return nil, err
				}
				gitLabExternalURL, err := config.API.GetGitLabExternalURL(ctx)
				if err != nil {
					return nil, err
				}

				return newGitRepositoryController(ctx, config.Log, config.API, agentID, gitLabExternalURL, gitRepositoryInformer, receiverInformer, projectReconciler, receiverClient, clientset.CoreV1())
			},
		},
	}, nil
}

func (f *Factory) Name() string {
	return flux.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}

func isFluxInstalled(ctx context.Context, log *zap.Logger, client apiextensionsv1client.ApiextensionsV1Interface) bool {
	for _, crd := range requiredFluxCRDs {
		ok, err := checkCRDExistsAndEstablished(ctx, client, crd)
		if err != nil {
			log.Error("Unable to check if CRD is installed", logz.K8sGroup(crd.Group), logz.Error(err))
			return false
		}
		if !ok {
			log.Debug("Required Flux CRD is not established", logz.K8sResource(crd.String()))
			return false
		}
	}
	return true
}

func checkCRDExistsAndEstablished(ctx context.Context, client apiextensionsv1client.ApiextensionsV1Interface, crd schema.GroupVersionResource) (bool, error) {
	obj, err := client.CustomResourceDefinitions().Get(ctx, crd.GroupResource().String(), metav1.GetOptions{})
	if err != nil {
		if kubeerrors.IsNotFound(err) {
			return false, nil
		}
		return false, fmt.Errorf("unable to get CRD %s: %w", crd.String(), err)
	}

	if !isCRDVersionServed(obj, crd.Version) {
		return false, nil
	}

	return isCRDEstablished(obj), nil
}

func isCRDVersionServed(obj *apiextensionsv1api.CustomResourceDefinition, version string) bool {
	// Check if the appropriate API version of the CRD is served.
	for _, v := range obj.Spec.Versions {
		if v.Served && v.Name == version {
			return true
		}
	}

	return false
}

func isCRDEstablished(obj *apiextensionsv1api.CustomResourceDefinition) bool {
	for _, cond := range obj.Status.Conditions {
		switch cond.Type { //nolint:exhaustive
		case apiextensionsv1api.Established:
			if cond.Status == apiextensionsv1api.ConditionTrue {
				return true
			}
			// we don't really care about any other conditions for now, because we don't own this CRD
			// and expect the owner to make sure it becomes established.
		}
	}
	return false
}
